package org.ben;

import org.knime.core.data.StringValue;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.DialogComponentNumberEdit;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelIntegerBounded;
import org.knime.core.node.defaultnodesettings.SettingsModelString;

/**
 * <code>NodeDialog</code> for the "Boltzman" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author 
 */
public class BoltzmanNodeDialog extends DefaultNodeSettingsPane {

    /**
     * New pane for configuring Boltzman node dialog.
     * This is just a suggestion to demonstrate possible default dialog
     * components.
     */
	

    private final SettingsModelString m_col_1 = 
		new SettingsModelString(BoltzmanNodeModel.CFG_COL_1, null);
                
    private SettingsModelInteger m_topscore = new SettingsModelInteger(
    		BoltzmanNodeModel.CFG_ITERATIONS, 2);
    
    private SettingsModelInteger m_cutoff = new SettingsModelInteger(
    		BoltzmanNodeModel.CFG_CUTOFF, 2);
    
    protected BoltzmanNodeDialog() {
        super();
        
        addDialogComponent(
        		new DialogComponentColumnNameSelection(
        			m_col_1, "Column 1",
        			0, false, StringValue.class)
        	);
        
        addDialogComponent(
        		new DialogComponentNumberEdit(m_topscore, "No. of top scores to use (-1 for all)", 15)
        	);
        
        addDialogComponent(
        		new DialogComponentNumberEdit(m_cutoff, "FingerPrint length cutoff (<10000)", 15)
        	);
        
    }
}

