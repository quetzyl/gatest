package org.ben;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "Pareto" Node.
 * 
 *
 * @author 
 */
public class ParetoNodeFactory 
        extends NodeFactory<ParetoNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public ParetoNodeModel createNodeModel() {
        return new ParetoNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<ParetoNodeModel> createNodeView(final int viewIndex,
            final ParetoNodeModel nodeModel) {
        return new ParetoNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new ParetoNodeDialog();
    }

}

