package org.ben;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "JungGA" Node.
 * 
 *
 * @author ben
 */
public class JungGANodeFactory 
        extends NodeFactory<JungGANodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public JungGANodeModel createNodeModel() {
        return new JungGANodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<JungGANodeModel> createNodeView(final int viewIndex,
            final JungGANodeModel nodeModel) {
        return new JungGANodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new JungGANodeDialog();
    }

}

