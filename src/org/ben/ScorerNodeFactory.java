package org.ben;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "Scorer" Node.
 * 
 *
 * @author 
 */
public class ScorerNodeFactory 
        extends NodeFactory<ScorerNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public ScorerNodeModel createNodeModel() {
        return new ScorerNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<ScorerNodeModel> createNodeView(final int viewIndex,
            final ScorerNodeModel nodeModel) {
        return new ScorerNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new ScorerNodeDialog();
    }

}

