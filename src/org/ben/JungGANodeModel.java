package org.ben;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.ben.CliqueFinderNodeModel.E;
import org.ben.CliqueFinderNodeModel.V;
import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.RowKey;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.defaultnodesettings.SettingsModelDouble;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelIntegerBounded;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.node.defaultnodesettings.SettingsModelStringArray;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeLogger;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import edu.uci.ics.jung.algorithms.scoring.BetweennessCentrality;
import edu.uci.ics.jung.algorithms.scoring.EigenvectorCentrality;
import edu.uci.ics.jung.algorithms.shortestpath.DistanceStatistics;
import edu.uci.ics.jung.algorithms.shortestpath.UnweightedShortestPath;
import edu.uci.ics.jung.algorithms.shortestpath.DijkstraDistance;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseGraph;
import edu.uci.ics.jung.graph.util.Pair;
import edu.uci.ics.jung.*;



/**
 * This is the model implementation of JungGA.
 * 
 *
 * @author 
 */
public class JungGANodeModel extends NodeModel {

	// the logger instance
	private static final NodeLogger logger = NodeLogger
	.getLogger(JungGANodeModel.class);


	public static final String CFG_POPULATION = "Population";

	public static final String CFG_ITERATIONS = "Iterations";

	public static final String CFG_SELPRES = "Selection Pressure";

	public static final String CFG_COL_1 = "Col1";

	public static final String CFG_COL_2 = "Col2";


	public static final String CFG_SFUNCTIONS = "scoring functions";


	public static final String CFG_SCORES = "No. of scores";


	public static final String CFG_PERCENTMUTATE = "percentage of mutations";


	public static final String CFG_TARGET = "maximum length to grow to";


	public static final String CFG_START = "starting subnet length";


	public static final String CFG_OUTFILE = "file name to write output";


	public static final String CFG_MINSIZE = "minimum subnet size";


	

	//public static final String CFG_NOPROPS = "no. of scoring functions";

	private final SettingsModelString m_outputfile = 
		new SettingsModelString(CFG_OUTFILE, "default");
	
	private final SettingsModelString m_col_1 = 
		new SettingsModelString(CFG_COL_1, "col1");

	private final SettingsModelString m_col_2 = 
		new SettingsModelString(CFG_COL_2, "col2");

	private SettingsModelInteger m_size_of_pop = new SettingsModelInteger(
			CFG_POPULATION, 2);

	private SettingsModelInteger m_iterations = new SettingsModelInteger(
			CFG_ITERATIONS, 2);

	private SettingsModelDouble m_sel_pres = new SettingsModelDouble(
			CFG_SELPRES, 2);

	private SettingsModelInteger m_scores = new SettingsModelInteger(
			CFG_SCORES, 2);
	
	private SettingsModelInteger m_minsize = new SettingsModelInteger(
			CFG_MINSIZE, 2);
	
	private SettingsModelStringArray m_sfunctions = 
		new SettingsModelStringArray(CFG_SFUNCTIONS, null);
	
	private SettingsModelDouble m_percentmutate = new SettingsModelDouble(
			CFG_PERCENTMUTATE, 2);
	
	private SettingsModelInteger m_targetsubnet = new SettingsModelInteger(
			CFG_TARGET, 2);
	
	private SettingsModelInteger m_startsubnet = new SettingsModelInteger(
			CFG_START, 2);
	
	//private SettingsModelInteger m_no_props = new SettingsModelInteger(
	//		CFG_NOPROPS, 1);


	/**
	 * Constructor for the node model.
	 */
	protected JungGANodeModel() {

		super(1, 1);
	}

	class Edge {
		private int idno;
		
		public Edge(int idno){
			this.idno=idno;
		}
		
		public int getidno() {return idno;}
		
	}
	
	class Node {
		private String nodeid;
		private double[] score =new double[m_scores.getIntValue()];

		public Node(String nodeid){
			this.nodeid=nodeid;
			
		}
		
		public String getnodeid() {return nodeid;}
		public double getscore(int i) {return score[i];}
		
		
		public void setscore(int i,double score) {this.score[i]=score;}
		

		
	}

	class Subnet {
		private ArrayList<Node> subset;
		private double[] score =new double[m_scores.getIntValue()];
		private int rank;

		public Subnet(ArrayList<Node> subset,int rank){
			this.subset =subset;
			this.rank=rank;
		}

		public int getrank() {return rank;}
		public double[] getscore() {return score;}
		public ArrayList<Node> getsubset() {return subset;}

		public void setrank(int rank) {this.rank=rank;}
		public void setscore(double[] score) {this.score=score;}

	}


	/**
	 * {@inheritDoc}
	 */
	 @Override
	 protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
			 final ExecutionContext exec) throws Exception {

		 try{
		
			Date newdate = new Date();
			long starttime = newdate.getTime();

			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			DateFormat writeDate = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
			
			PrintWriter writer1 = new PrintWriter(new FileWriter(m_outputfile.getStringValue()+"/JungGA."+writeDate.format(newdate)+".txt", true));
		
			writer1.println("Starting job at "+ dateFormat.format(newdate));
			System.out.println("Starting job at "+ dateFormat.format(newdate));
			
			//GetNetwork();
		 //GenerateNodeList();
		 //CalculateDegree();

		 int net_col_1 = inData[0].getDataTableSpec().findColumnIndex(
				 m_col_1.getStringValue());
		 int net_col_2 = inData[0].getDataTableSpec().findColumnIndex(
				 m_col_2.getStringValue());
		 
		// writer1.println("column index "+inData[0].getDataTableSpec().findColumnIndex(
		//		 m_col_2.getStringValue()));

		
		 

		 SparseGraph<Node, Edge> graph = new SparseGraph<Node, Edge>();
		 
		 int ecount =0;
				 
		 for (DataRow rgnRow : inData[0])
		 {
			 exec.checkCanceled();
			 String s1 = rgnRow.getCell(net_col_1).toString();
			 String s2 = rgnRow.getCell(net_col_2).toString();
			 
			 Node v1 = new Node(s1);
			 Node v2 = new Node(s2);
			 			 
			 for (Node test: graph.getVertices())
			 {
				 if(test.getnodeid().equals(s1)) v1=test;
				 if(test.getnodeid().equals(s2)) v2=test;
			 }
			 			 
			 Edge e =new Edge(ecount);
						
			 graph.addEdge(e, v1, v2);
			 ++ecount;
			 if(ecount%10000==0) System.out.print(ecount);
			 
			 
		 }
		 
		 //writer1.println(graph.toString());
			
		 writer1.println();
		 
		System.out.println("no. of nodes is "+graph.getVertexCount());
		System.out.println("no. of edges is "+graph.getEdgeCount());
		 
		 writer1.println("no. of nodes is "+graph.getVertexCount());
		 writer1.println("no. of edges is "+graph.getEdgeCount());
		
		 //for(Node test:nodeArray) writer1.print(test.getnodeid()+" ");

		 //GenerateInitialPopulation();

		 ArrayList<Subnet> population = new ArrayList<Subnet>();

		 int startingsubsetlength=m_startsubnet.getIntValue();

		 int ii =0;
		 while( ii<m_size_of_pop.getIntValue())
		 {
			 ArrayList<Node> tempnodeArray = new ArrayList<Node>();
			 for(Node temp :graph.getVertices()) tempnodeArray.add(temp);

			 Random generator1 = new Random();

			 ArrayList<Node> subset = new ArrayList<Node>();

			 for(int j=0;j<startingsubsetlength;++j)
			 {
				 int rand1 = generator1.nextInt(tempnodeArray.size());
				 subset.add(tempnodeArray.get(rand1));
				 tempnodeArray.remove(rand1);
			 }
			
			 Subnet subnet = new Subnet(subset,0);
			 if(!IdentityTest(subnet,population,exec))
			 {
			 population.add(subnet);
			 ++ii;
			 }
		 }

		 System.out.println("population is "+population.size());
		//Score node based functions and associate with nodes

		 String[] sfunctions=m_sfunctions.getStringArrayValue();
		 
		 for(String s :sfunctions)
		 {
			 if((s.equalsIgnoreCase("SumBetweenness"))||(s.equalsIgnoreCase("AvBetweenness")))
			 {
				 BetweennessCentrality<Node,Edge> bt = new BetweennessCentrality<Node,Edge>(graph);
				 for(Node n:graph.getVertices())
				 {
					 n.setscore(0, bt.getVertexScore(n));
				 }
			 }
			 if((s.equalsIgnoreCase("SumEigenCent"))||(s.equalsIgnoreCase("AvEigenCent")))
			 {
				 EigenvectorCentrality<Node,Edge> et = new EigenvectorCentrality<Node,Edge>(graph);
				 for(Node n:graph.getVertices())
				 {
					 n.setscore(1, et.getVertexScore(n));
				 }
			 }
			 if(s.equalsIgnoreCase("Diameter"))
			 {
				 System.out.println("Pre-diameter");
				 //DijkstraDistance<Node, Edge> dist = new DijkstraDistance<Node, Edge>(graph);
				UnweightedShortestPath<Node, Edge> uwsp = new UnweightedShortestPath<Node, Edge>(graph);
										
				uwsp.reset();
				
				System.out.println("diameter is "+DistanceStatistics.diameter(graph,uwsp,true));
				writer1.println("diameter is "+DistanceStatistics.diameter(graph,uwsp,true));
			 }
			 
		 }
		 
		 //for(subnet in InitialPopulation)
		 //ScoreSubnet(subnet);
		 
		 for(Subnet subnet:population)
		 {
			 double[] score =new double[m_scores.getIntValue()];
			 
			 for(int k=0; k<m_scores.getIntValue();++k)
			 {
				 score[k]=scorefunction(subnet,sfunctions[k],graph,exec);
			 }
			 
			 subnet.setscore(score);
		 }

		 //ParetoRankSubnets(InitialPopulation);
		 

		 ArrayList<double[]> data = new ArrayList<double[]>();

		 for (Subnet subnet:population)
		 {
			 double[]scorearray= subnet.getscore();
			 data.add(scorearray);				    	
		 }

		 int res[] = paretoRank(data, m_scores.getIntValue(), exec);

		 for (int p = 0; p < population.size(); ++p)
		 {
			 population.get(p).setrank(res[p]);		    	
		 }

		 
		 for(Subnet trial:population) {
			 writer1.print(trial.getrank()+" , ");
			 for(double score:trial.getscore())writer1.print(score+" , ");
			 writer1.print(trial.getsubset().size()+" : ");
			 for(Node test:trial.getsubset()) writer1.print(test.getnodeid()+" ");
			 writer1.println();
			 
		 }
		 
		 Date newdate1 = new Date();
		long time1 = newdate1.getTime();
		long delta1 = (time1 - starttime)/1000;

			writer1.println("Time to initialise the starting population and databases was " + delta1+" s");
			System.out.println("Time to initialise the starting population and databases was " + delta1+" s");
			

		 
		 
		 //while(GArunning)

		 //GenerateNewSubnet()

		 //	if(mutate)

		 //		RouletteWheelSelect(subnet1);
		 //		MutateSubnet(subnet1);

		 //	else if(crossover)

		 //		RouletteWheelSelect(subnet1);
		 //		RouletteWheelSelect(subnet2);
		 //		Crossover(subnet1,subnet2);


		 //	ScoreSubnet();
		 //	RemovePoorSubnet();

		int GArunning = 0;

		 while(GArunning<m_iterations.getIntValue())
		 {
			 int GeneratePopulation = 0;
			 ArrayList<Subnet> totpopulation = new ArrayList<Subnet>();
			 for(Subnet subnet:population) totpopulation.add(subnet);
			 
			 //generate double size population
			 
			 while(GeneratePopulation<m_size_of_pop.getIntValue())
			 {
				 Random generator = new Random();
			 	double randomOne = generator.nextDouble();
			 	Subnet newNet = null; 
			 	
			 	if(randomOne<m_percentmutate.getDoubleValue())
			 	{
				 	Subnet currentNet = population.get(roulettewheel(population,exec));
				 
				 	if(currentNet.getsubset().size()>m_targetsubnet.getIntValue())
				 	{
				 		newNet = ShrinkMutate(currentNet, graph.getVertices(),exec);
				 	}
				 	else if(currentNet.getsubset().size()>m_minsize.getIntValue())
				 	{
				 		newNet = Mutate(currentNet, graph.getVertices(),exec);
				 	}
				 	else
				 	{
				 		newNet = GrowMutate(currentNet, graph.getVertices(),exec);
				 	}
				 	
				 
			 	}
			 	else
			 	{
				 	Subnet currentNet = population.get(roulettewheel(population,exec));
				 
				 	if(currentNet.getsubset().size()<m_minsize.getIntValue())
				 	{
				 		newNet = GrowMutate(currentNet, graph.getVertices(),exec);
				 	}
				 	else 
				 	newNet = Crossover(currentNet,population,exec);
			 	}
			 
			 	if(!IdentityTest(newNet,totpopulation,exec))
			 	{
			 		double[] score =new double[m_scores.getIntValue()];
					for(int k=0; k<m_scores.getIntValue();++k)
					 {
						 score[k]=scorefunction(newNet,sfunctions[k],graph,exec);
					 }
					 
					 newNet.setscore(score);
			 		
			 		totpopulation.add(newNet);
				 	++GeneratePopulation;
			 	}
			 }
			 
			  // rank the doubled population
			 
			 ArrayList<double[]> data1 = new ArrayList<double[]>();

			 for (Subnet subnet:totpopulation)
			 {
				  data1.add(subnet.getscore());				    	
			 }

			 int res1[] = paretoRank(data1, m_scores.getIntValue(), exec);

			 for (int p = 0; p < totpopulation.size(); ++p)
			 {
				 totpopulation.get(p).setrank(res1[p]);		    	
			 }
			 
			 //reduce back to single population
			 
			 population.clear();
			 
			 int[] count =new int[totpopulation.size()];
				
			writer1.println("totpopulation = "+totpopulation.size());
				
				
				for (Subnet novel : totpopulation)
				{
				
					CountLoop:
					for (int i2=0;i2<totpopulation.size();++i2)
					{
						if(novel.getrank()==i2)
						{
						++count[i2];
						break CountLoop;
						}
					}
				}
				
				int finalcount =0;
				int track =0;
				
				TrackLoop:
				for (int i2=0;i2<totpopulation.size();++i2)
				{
					writer1.println("Rank = "+i2+" and count = "+ count[i2]);
					
					track=track+count[i2];
					if(track>m_size_of_pop.getIntValue()) 
					{
						finalcount=i2;
						break TrackLoop;
					}
					
				}
				
				int residue = m_size_of_pop.getIntValue()+count[finalcount] - track ;
				int rr =0;
				
				
				writer1.println("track = "+track);
				writer1.println("finalcount = " +finalcount);
				writer1.println("residue = " +residue);
				
				Collections.shuffle(totpopulation);
				
											
				for (Subnet novel : totpopulation)
				{
					if(novel.getrank()<finalcount) 
					{
					population.add(novel);
					
					}
					else if(novel.getrank()==finalcount)
						{
						if(rr<residue)
							{
							population.add(novel);
							
							}
						++rr;
						}
				}
							
				totpopulation.clear();
				
				//score the new population
				
			/*	for(Subnet sn:population)
				{
					double[] score =new double[m_scores.getIntValue()];
					for(int k=0; k<m_scores.getIntValue();++k)
					 {
						 score[k]=scorefunction(sn,sfunctions[k],graph,exec);
					 }
					 
					 sn.setscore(score);
				}*/
				
				
				++GArunning;
				System.out.println(GArunning+" ");
				writer1.println("Generation "+GArunning+" ");
				 writer1.println("no. of nodes is "+graph.getVertexCount());
				 writer1.println("no. of edges is "+graph.getEdgeCount());
				for(Subnet trial:population) {
					 writer1.print(GArunning+" , ");
					 writer1.print(trial.getrank()+" , ");
					 for(double score:trial.getscore())writer1.print(score+" , ");
					 writer1.print(trial.getsubset().size()+" : ");
					 for(Node test:trial.getsubset()) writer1.print(test.getnodeid()+" ");
					 writer1.println();
					 
				 }
				
		
				
		 }
		 
		 //rank the final population
		 
		 ArrayList<double[]> data1 = new ArrayList<double[]>();

		 for (Subnet subnet:population)
		 {
			  data1.add(subnet.getscore());				    	
		 }

		 int res1[] = paretoRank(data1, m_scores.getIntValue(), exec);

		 for (int p = 0; p < population.size(); ++p)
		 {
			 population.get(p).setrank(res1[p]);		    	
		 }
		 
		 
		 //Output final results set
		 
		 
		 
		 for(Subnet trial:population) {
			 writer1.print(trial.getrank()+" , ");
			 for(double score:trial.getscore())writer1.print(score+" , ");
			 writer1.print(trial.getsubset().size()+" : ");
			 for(Node test:trial.getsubset()) writer1.print(test.getnodeid()+" ");
			 writer1.println();
			 
		 }
		 	


			 // the data table spec of the single output table, 
			 // the table will have three columns:
			 DataColumnSpec[] allColSpecs = new DataColumnSpec[3+m_scores.getIntValue()];
			 allColSpecs[0] = 
				 new DataColumnSpecCreator("Subnet", StringCell.TYPE).createSpec();
			 allColSpecs[1] = 
				 new DataColumnSpecCreator("Length", IntCell.TYPE).createSpec();
			 allColSpecs[2] = 
				 new DataColumnSpecCreator("Rank", IntCell.TYPE).createSpec();
			 for(int m=0;m<m_scores.getIntValue();++m)
			 {
				 allColSpecs[3+m] = 
					 new DataColumnSpecCreator("Score "+m, DoubleCell.TYPE).createSpec();
			 }
			 
			 DataTableSpec outputSpec = new DataTableSpec(allColSpecs);
			 // the execution context will provide us with storage capacity, in this
			 // case a data container to which we will add rows sequentially
			 // Note, this container can also handle arbitrary big data tables, it
			 // will buffer to disc if necessary.
			 BufferedDataContainer container = exec.createDataContainer(outputSpec);
			 // let's add m_count rows to it
			 for (int i = 0; i < m_size_of_pop.getIntValue(); i++) {
				 
				 Subnet outputnet = population.get(i);
				 String subsettostring = " ";
				 
				 ArrayList<Node> subset =outputnet.getsubset();
				 
				 //for(Node test:subset) writer1.print(test.getnodeid()+" ");
				 //writer1.println();	
				 
					for(Node node:subset)
					{
						subsettostring=subsettostring.concat(node.getnodeid()+",");
					}
				 writer1.println("string is " +subsettostring);
				 
				 RowKey key = new RowKey("Row " + i);
				 // the cells of the current row, the types of the cells must match
				 // the column spec (see above)
				 DataCell[] cells = new DataCell[3+m_scores.getIntValue()];
				 cells[0] = new StringCell(subsettostring); 
				 cells[1] = new IntCell(outputnet.getsubset().size()); 
				 cells[2] = new IntCell(outputnet.getrank());
				 for(int m=0;m<m_scores.getIntValue();++m)
				 {
					 cells[3+m]= new DoubleCell(outputnet.getscore()[m]);
				 }
				 DataRow row = new DefaultRow(key, cells);
				 container.addRowToTable(row);

				 // check if the execution monitor was canceled
				 exec.checkCanceled();
				 exec.setProgress(i / (double)3, 
						 "Adding row " + i);
			 }
			 // once we are done, we close the container and return its table
			 container.close();
			 BufferedDataTable out = container.getTable();
			
			 
		Date newdate2 = new Date();
		long time2 = newdate2.getTime();
		long delta2 = (time2 - starttime)/1000;

		writer1.println("Total time was " + delta2+" s");
		System.out.println("Total time was " + delta2+" s");
		
		writer1.close();
		return new BufferedDataTable[]{out};
			 
	 } catch (Exception ex) {
		            ex.printStackTrace();
		            throw ex;
		        }
	 }
	 
	 
		 /**
		  * {@inheritDoc}
		  */
		 @Override
		 protected void reset() {
			 // TODO Code executed on reset.
			 // Models build during execute are cleared here.
			 // Also data handled in load/saveInternals will be erased here.
		 }

		 /**
		  * {@inheritDoc}
		  */
		 @Override
		 protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
		 throws InvalidSettingsException {

			 // TODO: check if user settings are available, fit to the incoming
			 // table structure, and the incoming types are feasible for the node
			 // to execute. If the node can execute in its current state return
			 // the spec of its output data table(s) (if you can, otherwise an array
			 // with null elements), or throw an exception with a useful user message

			 return new DataTableSpec[]{null};
		 }

		 /**
		  * {@inheritDoc}
		  */
		 @Override
		 protected void saveSettingsTo(final NodeSettingsWO settings) {

			 // TODO save user settings to the config object.


			 m_col_1.saveSettingsTo(settings);
			 m_col_2.saveSettingsTo(settings);
			 m_iterations.saveSettingsTo(settings);
			 m_sel_pres.saveSettingsTo(settings);
			 m_size_of_pop.saveSettingsTo(settings);
			 m_scores.saveSettingsTo(settings);
			 m_sfunctions.saveSettingsTo(settings);
			 m_percentmutate.saveSettingsTo(settings);
			 m_targetsubnet.saveSettingsTo(settings);
			 m_startsubnet.saveSettingsTo(settings);
			 m_outputfile.saveSettingsTo(settings);
		 }

		 /**
		  * {@inheritDoc}
		  */
		 @Override
		 protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
		 throws InvalidSettingsException {

			 // TODO load (valid) settings from the config object.
			 // It can be safely assumed that the settings are valided by the 
			 // method below.


			 m_col_1.loadSettingsFrom(settings);
			 m_col_2.loadSettingsFrom(settings);
			 m_iterations.loadSettingsFrom(settings);
			 m_sel_pres.loadSettingsFrom(settings);
			 m_size_of_pop.loadSettingsFrom(settings);
			 m_scores.loadSettingsFrom(settings);
			 m_sfunctions.loadSettingsFrom(settings);
			 m_percentmutate.loadSettingsFrom(settings);
			 m_targetsubnet.loadSettingsFrom(settings);
			 m_startsubnet.loadSettingsFrom(settings);
			 m_outputfile.loadSettingsFrom(settings);
		 }

		 /**
		  * {@inheritDoc}
		  */
		 @Override
		 protected void validateSettings(final NodeSettingsRO settings)
		 throws InvalidSettingsException {

			 // TODO check if the settings could be applied to our model
			 // e.g. if the count is in a certain range (which is ensured by the
			 // SettingsModel).
			 // Do not actually set any values of any member variables.


			 m_col_1.validateSettings(settings);
			 m_col_2.validateSettings(settings);
			 m_iterations.validateSettings(settings);
			 m_sel_pres.validateSettings(settings);
			 m_size_of_pop.validateSettings(settings);
			 m_scores.validateSettings(settings);
			 m_sfunctions.validateSettings(settings);
			 m_percentmutate.validateSettings(settings);
			 m_targetsubnet.validateSettings(settings);
			 m_startsubnet.validateSettings(settings);
			 m_outputfile.validateSettings(settings);
		 }

		 /**
		  * {@inheritDoc}
		  */
		 @Override
		 protected void loadInternals(final File internDir,
				 final ExecutionMonitor exec) throws IOException,
				 CanceledExecutionException {

			 // TODO load internal data. 
			 // Everything handed to output ports is loaded automatically (data
			 // returned by the execute method, models loaded in loadModelContent,
			 // and user settings set through loadSettingsFrom - is all taken care 
			 // of). Load here only the other internals that need to be restored
			 // (e.g. data used by the views).

		 }

		 /**
		  * {@inheritDoc}
		  */
		 @Override
		 protected void saveInternals(final File internDir,
				 final ExecutionMonitor exec) throws IOException,
				 CanceledExecutionException {

			 // TODO save internal models. 
			 // Everything written to output ports is saved automatically (data
			 // returned by the execute method, models saved in the saveModelContent,
			 // and user settings saved through saveSettingsTo - is all taken care 
			 // of). Save here only the other internals that need to be preserved
			 // (e.g. data used by the views).

		 }



		 private int[] paretoRank(ArrayList<double[]> data, 
				 int noprop,
				 final ExecutionContext exec) throws CanceledExecutionException
		 {
			 int[] res = new int[data.size()];
			 int[] dominated_by = new int[data.size()]; //solution at i is dominated by dominatedBy[i] other solutions
			 List<Integer> curr_front = new ArrayList<Integer>();
			 Map<Integer,ArrayList<Integer>> dominates = new HashMap<Integer,ArrayList<Integer>>();

			 for (int i = 0; i < data.size(); ++i)
			 {
				 for (int j = i + 1; j < data.size(); ++j)
				 {
					 int dom1 = 0, dom2 = 0;
					 for (int p = 0; p < noprop; ++p)
					 {
						 if (data.get(i)[p] < data.get(j)[p]) dom1 += 1;
						 else if (data.get(i)[p] > data.get(j)[p]) dom2 += 1;
					 }
					 if (dom1 > 0 && dom2 == 0)
					 {
						 dominated_by[j] += 1;
						 if (null == dominates.get(i))
							 dominates.put(i, new ArrayList<Integer>());
						 dominates.get(i).add(j);
					 }
					 else if (dom2 > 0 && dom1 == 0)
					 {
						 dominated_by[i] += 1;
						 if (null == dominates.get(j))
							 dominates.put(j, new ArrayList<Integer>());
						 dominates.get(j).add(i);
					 }
					 exec.setProgress((data.size() + i*data.size() + j)/2);
					 exec.checkCanceled();
				 }
				 if (0 == dominated_by[i]) curr_front.add(i);
			 }

			 int rank = 1;
			 while (0 < curr_front.size())
			 {
				 List<Integer> new_front = new ArrayList<Integer>();
				 for (int i : curr_front)
				 {
					 res[i] = rank;
					 List<Integer> dom_ind = dominates.get(i);
					 if (null != dom_ind)
						 for (int di : dom_ind)
						 {
							 dominated_by[di] -= 1;
							 if (0 == dominated_by[di]) new_front.add(di);
						 }
				 }
				 curr_front = new_front;
				 rank += 1;
			 }

			 return res;
		 }
		 
		 private double scorefunction(Subnet subnet , String scoreid,SparseGraph<Node, Edge> graph,
				 final ExecutionContext exec) throws CanceledExecutionException
		 {
			double score =0;
			SparseGraph<Node, Edge> graph1 =new SparseGraph<Node, Edge>();
			for(Edge e: graph.getEdges())
			{
				Pair<Node> n1=graph.getEndpoints(e);
				
				graph1.addEdge(e, n1.getFirst(), n1.getSecond());
			}
			
			
			if(scoreid.equalsIgnoreCase("SumofDegree"))
			{
				ArrayList<Node> subset =subnet.getsubset();
				for(Node node:subset)
				{
					score =score+graph1.degree(node);
				}
				score = -score;
			}
			else if(scoreid.equalsIgnoreCase("AverageDegree"))
			{
				ArrayList<Node> subset =subnet.getsubset();
				int count=0;
				for(Node node:subset)
				{
					score =score+graph1.degree(node);
					++count;
				}
				score=-score/count;
			}
			else if(scoreid.equalsIgnoreCase("MaxDegree"))
			{
				ArrayList<Node> subset =subnet.getsubset();
				
				for(Node node:subset)
				{
					if(graph1.degree(node)>score) score =graph1.degree(node);
				}
				score = -score;
			}
			else if(scoreid.equalsIgnoreCase("Diameter"))
			{
				for(Node node: subnet.getsubset())
				{
					graph1.removeVertex(node);
				}
				//System.out.print(graph1.getEdgeCount()+" , ");
				//System.out.print(graph1.getVertexCount()+" , ");
				
				
				UnweightedShortestPath<Node, Edge> uwsp = new UnweightedShortestPath<Node, Edge>(graph1);
				uwsp.reset();
				
				double diameter = 0;
		        
		        for(Node v : graph1.getVertices()) {
		            for(Node w : graph1.getVertices()) {

		                if (v.equals(w) == false) // don't include self-distances
		                {
		                    Number dist = uwsp.getDistance(v, w);
		                    if (dist == null)
		                    {
		                       //return Double.POSITIVE_INFINITY;
		                    }
		                    else
		                        diameter = Math.max(diameter, dist.doubleValue());
		                }
		            }
		        }
		       //System.out.println(diameter);
		        
		        score = diameter;
				
				
				
				//score = DistanceStatistics.diameter(graph1,uwsp,true/*,new DijkstraDistance<Node, Edge>(graph)*/);
			}
			
			else if(scoreid.equalsIgnoreCase("SubnetSize"))
			{
				score = subnet.getsubset().size();
			}
			
			else if(scoreid.equalsIgnoreCase("SumBetweenness"))
			{
				ArrayList<Node> subset =subnet.getsubset();
				for(Node node:subset)
				{
					score =score+node.getscore(0);
				}
							
			}
			else if(scoreid.equalsIgnoreCase("AvBetweenness"))
			{
				ArrayList<Node> subset =subnet.getsubset();
				int count=0;
				for(Node node:subset)
				{
					score =score+node.getscore(0);
					++count;
				}
				score=-score/count;
				
			}
			else if(scoreid.equalsIgnoreCase("SumEigenCent"))
			{
				ArrayList<Node> subset =subnet.getsubset();
				for(Node node:subset)
				{
					score =score+node.getscore(1);
				}
							
			}
			else if(scoreid.equalsIgnoreCase("AvEigenCent"))
			{
				ArrayList<Node> subset =subnet.getsubset();
				int count=0;
				for(Node node:subset)
				{
					score =score+node.getscore(1);
					++count;
				}
				score=-score/count;
				
			}
			else if(scoreid.equalsIgnoreCase("AvPathLen"))
			{
				for(Node node: subnet.getsubset())
				{
					
				}
				//System.out.print(graph1.getEdgeCount()+" , ");
				//System.out.print(graph1.getVertexCount()+" , ");
				
				
				
				score = 0;
			}
			else if(scoreid.equalsIgnoreCase(""))
			{
				score = 0;
			}
			
			return score;
		 }
		 
		 private int roulettewheel(ArrayList<Subnet> population,
				 final ExecutionContext exec) throws CanceledExecutionException
		 {
			 int setp =0;
			 Random generator = new Random();

			 int[] countofsol = new int[m_size_of_pop.getIntValue()+1];
			 Arrays.fill(countofsol, 0);
			 for (int p = 0; p < m_size_of_pop.getIntValue(); ++p)
			 {
				 for (int q= 1; q<  m_size_of_pop.getIntValue(); ++q)
				 {
					 if(population.get(p).getrank()==q) ++countofsol[q];
				 }

			 }
			 double[] rcount = new double[m_size_of_pop.getIntValue()+1];
			 for (int p = 0; p < m_size_of_pop.getIntValue(); ++p)
			 {

				 if(population.get(p).getrank()<=m_size_of_pop.getIntValue()) rcount[p]= 1+ countofsol[population.get(p).getrank()];
				 for (int q= 1; q<population.get(p).getrank()-1; ++q)
				 {
					 rcount[p] = rcount[p]+ 2* countofsol[q];
				 }

			 }    



			 double wheel = 0;

			 for (int p = 0; p < m_size_of_pop.getIntValue(); ++p)
			 {
				 wheel = wheel + (m_sel_pres.getDoubleValue()*(m_size_of_pop.getIntValue()+1-rcount[p]) 
						 + countofsol[p] -2)/(m_size_of_pop.getIntValue()*(m_size_of_pop.getIntValue()-1));

				 //writer1.println("Molecule " + p + " is Pareto rank " + molarray.get(p).res);
				 //wheel = wheel + 1/molarray.get(p).res;			    	
			 }

			 double randomOne = generator.nextDouble();

			 double randomWheel = wheel*randomOne;

			 //writer1.println("wheel= "+wheel);
			 //writer1.println("randomwheel= "+randomWheel);

			 double testwheel =0;
			 
			 for (int p = 0; p < m_size_of_pop.getIntValue(); ++p)
			 {
				 testwheel = testwheel + (m_sel_pres.getDoubleValue()*(m_size_of_pop.getIntValue()+1-rcount[p])
						 + countofsol[p] -2)/(m_size_of_pop.getIntValue()*(m_size_of_pop.getIntValue()-1));
				 if (randomWheel <testwheel)
				 {
					 setp = p;

					 p= m_size_of_pop.getIntValue() +1;

				 }

			 }
			 return setp;	

		 }
		 
		 private boolean IdentityTest(Subnet subnet, ArrayList<Subnet> population,
		  		final ExecutionContext exec) throws CanceledExecutionException
		  {
			 boolean it =false;
			 //System.out.print("ID test ");
			 
			 Poploop:
			 for(Subnet testnet: population)
			 {
				 if((testnet.getsubset().size()!=subnet.getsubset().size())) continue Poploop;
				 else 
				 {
					 Subloop:
					 for(Node sn:subnet.getsubset())
					 {
						if(testnet.getsubset().contains(sn)) continue Subloop;
						continue Poploop;
					 }
				     
					 
					 it=true;
					// System.out.print(subnet.getsubset().size()+" ");
					// for(Node test1:subnet.getsubset()) System.out.print(test1.getnodeid()+" ");
					// System.out.println("equals ");	
					// System.out.print(testnet.getsubset().size()+" ");
					// for(Node test2:testnet.getsubset()) System.out.print(test2.getnodeid()+" ");
					// System.out.println("eliminated ");		 
					 
					 break Poploop;
				 }
				 
			 }
		
			 return it;
		  }
		  
		 private Subnet Mutate(Subnet currentNet,Collection<Node> nodeArray,
				 final ExecutionContext exec) throws CanceledExecutionException
		  {
			 ArrayList<Node> subset = new ArrayList<Node>() ;
			 for(Node n:currentNet.getsubset())
				 subset.add(n);
			 
			 Random generator = new Random();
			 double third = generator.nextDouble();
			 
			 if((third<0.333333)&&(subset.size()>1))
			 {
				 int rand = generator.nextInt(subset.size());
				 subset.remove(rand);
			 }
			 else if(third<0.666666)
			 {
				 ArrayList<Node> tempnodeArray = new ArrayList<Node>();
				 for(Node temp :nodeArray) tempnodeArray.add(temp);
				 for(Node temp1 : subset) tempnodeArray.remove(temp1);

				 int rand1 = generator.nextInt(tempnodeArray.size());
				 
				 subset.add(tempnodeArray.get(rand1));
			 }
			 else
			 {	 
				 int rand = generator.nextInt(subset.size());
			 
				 ArrayList<Node> tempnodeArray = new ArrayList<Node>();
				 for(Node temp :nodeArray) tempnodeArray.add(temp);
				 for(Node temp1 : subset) tempnodeArray.remove(temp1);

				 int rand1 = generator.nextInt(tempnodeArray.size());
				 subset.remove(rand);
				 subset.add(tempnodeArray.get(rand1));
			 }
			 
			 Subnet newNet = new Subnet(subset,0);
			 
			 return newNet;
		  }
		 
		 private Subnet ShrinkMutate(Subnet currentNet,Collection<Node> nodeArray,
				 final ExecutionContext exec) throws CanceledExecutionException
		  {
			 ArrayList<Node> subset = new ArrayList<Node>() ;
			 for(Node n:currentNet.getsubset())
				 subset.add(n);
			 
			 Random generator = new Random();
			 double third = generator.nextDouble();
			 
			 if((third<0.66666)&&(subset.size()>1))
			 {
				 int rand = generator.nextInt(subset.size());
				 subset.remove(rand);
			 }
			 else
			 {	 
			 int rand = generator.nextInt(subset.size());
			 
			 ArrayList<Node> tempnodeArray = new ArrayList<Node>();
			 for(Node temp :nodeArray) tempnodeArray.add(temp);
			 for(Node temp1 : subset) tempnodeArray.remove(temp1);

			 int rand1 = generator.nextInt(tempnodeArray.size());
			 subset.remove(rand);
			 subset.add(tempnodeArray.get(rand1));
			 }
			 
			 Subnet newNet = new Subnet(subset,0);
			
			 return newNet;
		  }
		 
		 private Subnet GrowMutate(Subnet currentNet,Collection<Node> nodeArray,
				 final ExecutionContext exec) throws CanceledExecutionException
		  {
			 ArrayList<Node> subset = new ArrayList<Node>() ;
			 for(Node n:currentNet.getsubset())
				 subset.add(n);
			 
			 Random generator = new Random();
			 double third = generator.nextDouble();
			 
			 if((third<0.66666)&&(subset.size()>1))
			 {
				
				 
				 ArrayList<Node> tempnodeArray = new ArrayList<Node>();
				 for(Node temp :nodeArray) tempnodeArray.add(temp);
				 for(Node temp1 : subset) tempnodeArray.remove(temp1);

				 int rand1 = generator.nextInt(tempnodeArray.size());
				  subset.add(tempnodeArray.get(rand1));
			 }
			 else
			 {	 
			 int rand = generator.nextInt(subset.size());
			 
			 ArrayList<Node> tempnodeArray = new ArrayList<Node>();
			 for(Node temp :nodeArray) tempnodeArray.add(temp);
			 for(Node temp1 : subset) tempnodeArray.remove(temp1);

			 int rand1 = generator.nextInt(tempnodeArray.size());
			 subset.remove(rand);
			 subset.add(tempnodeArray.get(rand1));
			 }
			 
			 Subnet newNet = new Subnet(subset,0);
			
			 return newNet;
		  }
		 
		 private Subnet Crossover(Subnet currentNet,ArrayList<Subnet> population,
				 final ExecutionContext exec) throws CanceledExecutionException
		  {
			
				 
			 ArrayList<Node> subset = new ArrayList<Node>() ;
			 for(Node n:currentNet.getsubset())
				 subset.add(n);
			 
			 Random generator = new Random();
			 int rand =1;
			 if(subset.size()>1) 
				 {
				 rand = generator.nextInt(subset.size()-1)+1;
				 }
			 
			 
			 ArrayList<Subnet> tempPop = new ArrayList<Subnet>();
				for(Subnet temp:population) tempPop.add(temp); 
				 tempPop.remove(currentNet);
			 
			 int rand1 = generator.nextInt(tempPop.size());
			 
			 Subnet crossNet = tempPop.get(rand1);
			 ArrayList<Node> crossset  = new ArrayList<Node>() ;
			 for(Node n:crossNet.getsubset())
				 crossset.add(n);
			 
			 ArrayList<Node> newset = new ArrayList<Node>();
			 
			 if(rand>crossset.size()) rand = crossset.size();
			 
			 int tcount = Math.max(subset.size()-rand,crossset.size()-rand);
			 int tc=0;
			 for(int i=0;i<rand;++i)newset.add(subset.get(i));
			 
			 CrossLoop:
			 for(Node n: crossset)
				 {
				 if(!newset.contains(n)) 
					 {
					 newset.add(n);
					 ++tc;
					 if(tc>=tcount) break CrossLoop;
					 }
				 }
			 
			 Subnet newNet= new Subnet(newset,0);
			 
			 return newNet;
		  }
		 

		 
		 
	 }
