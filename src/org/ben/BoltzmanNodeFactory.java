package org.ben;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "Boltzman" Node.
 * 
 *
 * @author 
 */
public class BoltzmanNodeFactory 
        extends NodeFactory<BoltzmanNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public BoltzmanNodeModel createNodeModel() {
        return new BoltzmanNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<BoltzmanNodeModel> createNodeView(final int viewIndex,
            final BoltzmanNodeModel nodeModel) {
        return new BoltzmanNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new BoltzmanNodeDialog();
    }

}

