package org.ben;

import java.util.Arrays;

import javax.swing.ListSelectionModel;

import org.knime.core.data.StringValue;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentFileChooser;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.DialogComponentStringListSelection;
import org.knime.core.node.defaultnodesettings.SettingsModelDouble;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelIntegerBounded;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.node.defaultnodesettings.SettingsModelStringArray;

/**
 * <code>NodeDialog</code> for the "CliqueFinder" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author ben
 */
public class CliqueFinderNodeDialog extends DefaultNodeSettingsPane {

 
	private final SettingsModelString m_outputfile = 
		new SettingsModelString(CliqueFinderNodeModel.CFG_OUTFILE, null);
	
	private final SettingsModelString m_col_1 = 
    		new SettingsModelString(CliqueFinderNodeModel.CFG_COL_1, null);
    	
    	private final SettingsModelString m_col_2 = 
    		new SettingsModelString(CliqueFinderNodeModel.CFG_COL_2, null);
    	
    	
        protected CliqueFinderNodeDialog() {
            super();
            
            addDialogComponent(
            		new DialogComponentColumnNameSelection(
            			m_col_1, "Column 1",
            			0, false, StringValue.class)
            	);
            
            addDialogComponent(
            		new DialogComponentColumnNameSelection(
            			m_col_2, "Column 2",
            			0, false, StringValue.class)
            	);
            
           // addDialogComponent(
           // 		new DialogComponentNumber(m_values, "No. of values to read in", 1)
           // 	);
            String history = " ";
            
            addDialogComponent(
            		new DialogComponentFileChooser(m_outputfile,history, 0, false)
            	);
            
            
            
            
        }
}

