package org.ben;

import org.knime.core.data.StringValue;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;

import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;

import org.knime.core.node.defaultnodesettings.SettingsModelString;

/**
 * <code>NodeDialog</code> for the "Scorer" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author 
 */

public class ScorerNodeDialog extends DefaultNodeSettingsPane {

	
	private final SettingsModelString m_col_1 = 
		new SettingsModelString(ScorerNodeModel.CFG_COL_1, null);
	
	private final SettingsModelString m_col_2 = 
		new SettingsModelString(ScorerNodeModel.CFG_COL_2, null);
	
	private final SettingsModelBoolean m_smooth =
		new SettingsModelBoolean(ScorerNodeModel.CFG_SMOOTH, false);
	
    /**
     * New pane for configuring Scorer node dialog.
     * This is just a suggestion to demonstrate possible default dialog
     * components.
     */
    @SuppressWarnings("unchecked")
	protected ScorerNodeDialog() {
        super();
        
        addDialogComponent(
        		new DialogComponentColumnNameSelection(
        			m_col_1, "proteins",
        			0, false, StringValue.class)
        	);
        
        addDialogComponent(
        		new DialogComponentColumnNameSelection(
        			m_col_2, "cmpnd id",
        			0, false, StringValue.class)
        	);
               
        addDialogComponent(
        		new DialogComponentBoolean(
        			m_smooth, "Use Good_Turing")
        	);
        
    }
}

