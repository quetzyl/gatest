package org.ben;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.RowKey;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.data.container.SingleCellFactory;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.defaultnodesettings.SettingsModelFilterString;
import org.knime.core.node.defaultnodesettings.SettingsModelIntegerBounded;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeLogger;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;


/**
 * This is the model implementation of Pareto.
 * 
 *
 * @author 
 */
public class ParetoNodeModel extends NodeModel {
    
    // the logger instance
    private static final NodeLogger logger = NodeLogger
            .getLogger(ParetoNodeModel.class);
	public static final String CFG_COL_1 = "string";
        
	  private final SettingsModelFilterString m_col_1 = 
			new SettingsModelFilterString(CFG_COL_1);
    
    /**
     * Constructor for the node model.
     */
    protected ParetoNodeModel() {
    
        // TODO one incoming port and one outgoing port is assumed
        super(1, 1);
    }

  
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception {

       
    	try{
    		
    	int noprop = m_col_1.getIncludeList().size();
    	
    	int[] cols = new int[noprop];
    	
    	
    	for(int i=0;i<noprop;++i)
    	{
    		cols[i] = inData[0].getDataTableSpec().findColumnIndex(
					m_col_1.getIncludeList().get(i));
    	}

    	
    	ArrayList<double[]> data = new ArrayList<double[]>();
    	
    	
    	
    	for(DataRow rgnRow : inData[0])
		{
		double[] drow = new double[noprop];   	
		for(int i=0;i<noprop;++i)
    	{
    		drow[i] = 1-Double.parseDouble(rgnRow.getCell(cols[i]).toString());
    	}    
		data.add(drow);
		
		}
    	
    	int[] pscore = paretoRank( data, 
    			 noprop,
    			 exec);
    	
    	HashMap<RowKey,Integer> pmap = new HashMap<RowKey,Integer>();
    	int i=0;
    	
    	for(DataRow rgnRow : inData[0])
		{
		pmap.put(rgnRow.getKey(), pscore[i]);
		++i;
		}
    	
    	
    	
           	DataTableSpec inSpec = inData[0].getDataTableSpec();
            	ColumnRearranger rearranger = createColumnRearranger(inSpec,pmap);
            	
            	BufferedDataTable out = exec.createColumnRearrangeTable(
            		inData[0], rearranger, exec);
            	
        
            	
            	
            	
    	
    	
/*        DataColumnSpec[] allColSpecs = new DataColumnSpec[1];
        allColSpecs[0] = 
            new DataColumnSpecCreator("Pareto Rank", IntCell.TYPE).createSpec();
            
        
        DataTableSpec outputSpec = new DataTableSpec(allColSpecs);
        
        
        // the execution context will provide us with storage capacity, in this
        // case a data container to which we will add rows sequentially
        // Note, this container can also handle arbitrary big data tables, it
        // will buffer to disc if necessary.
        BufferedDataContainer container = exec.createDataContainer(outputSpec);
        // let's add m_count rows to it
        for (int i = 0; i < pscore.length; i++) {
            RowKey key = new RowKey("Row " + i);
            // the cells of the current row, the types of the cells must match
            // the column spec (see above)
            DataCell[] cells = new DataCell[1];
            cells[0]  = new IntCell(pscore[i]);
            DataRow row = new DefaultRow(key, cells);
            container.addRowToTable(row);
            
            // check if the execution monitor was canceled
            exec.checkCanceled();
            
        }
        // once we are done, we close the container and return its table
        container.close();
        BufferedDataTable out = container.getTable(); */
            	
        return new BufferedDataTable[]{out};
        
            } catch (Exception ex) {
    			ex.printStackTrace();
    			throw ex;
    		}
    }

    /**
     * {@inheritDoc}
     */
    

    private ColumnRearranger createColumnRearranger(
    		DataTableSpec spec, final HashMap<RowKey,Integer> pmap) throws InvalidSettingsException {
    	// check user settings against input spec here
    	// fail with InvalidSettingsException if invalid
        	ColumnRearranger result = new ColumnRearranger(spec);
    	// the following code appends a single column
    	DataColumnSpecCreator appendSpecCreator = 
    		new DataColumnSpecCreator("Pareto rank", IntCell.TYPE);
    	DataColumnSpec appendSpec = appendSpecCreator.createSpec();
        	result.append(new SingleCellFactory(appendSpec) {
    		public DataCell getCell(final DataRow row) {
    			DataCell resultCell = new IntCell(pmap.get(row.getKey()));
    			return resultCell;
    		}
        	});
    	return result;
    }

    
    
    
    private int[] paretoRank(ArrayList<double[]> data, 
			int noprop,
			final ExecutionContext exec) throws CanceledExecutionException
			{
		int[] res = new int[data.size()];
		int[] dominated_by = new int[data.size()]; //solution at i is dominated by dominatedBy[i] other solutions
		List<Integer> curr_front = new ArrayList<Integer>();
		Map<Integer,ArrayList<Integer>> dominates = new HashMap<Integer,ArrayList<Integer>>();

		for (int i = 0; i < data.size(); ++i)
		{
			for (int j = i + 1; j < data.size(); ++j)
			{
				int dom1 = 0, dom2 = 0;
				for (int p = 0; p < noprop; ++p)
				{
					if (data.get(i)[p] < data.get(j)[p]) dom1 += 1;
					else if (data.get(i)[p] > data.get(j)[p]) dom2 += 1;
				}
				if (dom1 > 0 && dom2 == 0)
				{
					dominated_by[j] += 1;
					if (null == dominates.get(i))
						dominates.put(i, new ArrayList<Integer>());
					dominates.get(i).add(j);
				}
				else if (dom2 > 0 && dom1 == 0)
				{
					dominated_by[i] += 1;
					if (null == dominates.get(j))
						dominates.put(j, new ArrayList<Integer>());
					dominates.get(j).add(i);
				}
				exec.setProgress((data.size() + i*data.size() + j)/10000);
				exec.checkCanceled();
			}
			if (0 == dominated_by[i]) curr_front.add(i);
		}

		int rank = 1;
		while (0 < curr_front.size())
		{
			List<Integer> new_front = new ArrayList<Integer>();
			for (int i : curr_front)
			{
				res[i] = rank;
				List<Integer> dom_ind = dominates.get(i);
				if (null != dom_ind)
					for (int di : dom_ind)
					{
						dominated_by[di] -= 1;
						if (0 == dominated_by[di]) new_front.add(di);
					}
			}
			curr_front = new_front;
			rank += 1;
		}

		return res;
	}
    
    @Override
    protected void reset() {
        // TODO Code executed on reset.
        // Models build during execute are cleared here.
        // Also data handled in load/saveInternals will be erased here.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException {
        
        // TODO: check if user settings are available, fit to the incoming
        // table structure, and the incoming types are feasible for the node
        // to execute. If the node can execute in its current state return
        // the spec of its output data table(s) (if you can, otherwise an array
        // with null elements), or throw an exception with a useful user message

        return new DataTableSpec[]{null};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings) {

        // TODO save user settings to the config object.
        
        m_col_1.saveSettingsTo(settings);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException {
            
        // TODO load (valid) settings from the config object.
        // It can be safely assumed that the settings are valided by the 
        // method below.
        
    	m_col_1.loadSettingsFrom(settings);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException {
            
        // TODO check if the settings could be applied to our model
        // e.g. if the count is in a certain range (which is ensured by the
        // SettingsModel).
        // Do not actually set any values of any member variables.

    	m_col_1.validateSettings(settings);

    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
        
        // TODO load internal data. 
        // Everything handed to output ports is loaded automatically (data
        // returned by the execute method, models loaded in loadModelContent,
        // and user settings set through loadSettingsFrom - is all taken care 
        // of). Load here only the other internals that need to be restored
        // (e.g. data used by the views).

    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
       
        // TODO save internal models. 
        // Everything written to output ports is saved automatically (data
        // returned by the execute method, models saved in the saveModelContent,
        // and user settings saved through saveSettingsTo - is all taken care 
        // of). Save here only the other internals that need to be preserved
        // (e.g. data used by the views).

    }

}

