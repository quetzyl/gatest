package org.ben;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.RowKey;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;

import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeLogger;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;


/**
 * This is the model implementation of Scorer.
 * 
 *
 * @author 
 */
public class ScorerNodeModel extends NodeModel {
    
    // the logger instance
    private static final NodeLogger logger = NodeLogger
            .getLogger(ScorerNodeModel.class);

	public static final String CFG_COL_1 = "column1";

	public static final String CFG_COL_2 = "column2";

	public static final String CFG_SMOOTH = "smooth";
        
    private final SettingsModelString m_col_1 = 
		new SettingsModelString(CFG_COL_1, "col1");
    
    private final SettingsModelString m_col_2 = 
		new SettingsModelString(CFG_COL_2, "col2");
    
    private final SettingsModelBoolean m_smooth =
		new SettingsModelBoolean(CFG_SMOOTH, false);
	

    /**
     * Constructor for the node model.
     */
    protected ScorerNodeModel() {
    
      
        super(3, 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception {

        logger.info("Node Model Stub... this is not yet implemented !");

        try{
      	  
        	Date newdate = new Date();
			long starttime = newdate.getTime();
        	

			SimpleDateFormat formatter = new SimpleDateFormat("yy.MM.dd.hh.mm");
			
			String output = formatter.format(starttime);
			
			boolean testing =false;
			
		    int net_col_2 = inData[0].getDataTableSpec().findColumnIndex(
    				 m_col_2.getStringValue());
			
            int net_col_1 = inData[0].getDataTableSpec().findColumnIndex(
          				 m_col_1.getStringValue());
            
            PrintWriter writer1 = new PrintWriter(new FileWriter("D:/EclipseRunOutputs/scorererrors."+output+".txt"), true);
            
            double toterr =0.0;
            int counterr=0;
            
            System.out.println(net_col_1+" , "+net_col_2+","+m_smooth.getBooleanValue());
            
          //  ArrayList<String> strings = new ArrayList<String>();
           // ArrayList<String> compounds = new ArrayList<String>();
          //  ArrayList<Integer> scores = new ArrayList<Integer>();
            
            TreeMap<Double,Double> node1 =new TreeMap<Double,Double>();
           
           // ArrayList<Double> score1 =new ArrayList<Double>();
                       
           // ArrayList<ArrayList<Double>> pout = new ArrayList<ArrayList<Double>>();
            
            SortedSet<Double> targets = new TreeSet<Double>();
            
            for(DataRow t:inData[2])
            {
            	String s1 = t.getCell(0).toString();
               	
        		StringTokenizer st1 = new StringTokenizer(s1,",");

    			while(st1.hasMoreTokens())
    			{
    				targets.add(Double.parseDouble(st1.nextToken()));
    			}
    			
            }
            
            for(DataRow boltrow : inData[1])
            {
            	Double dbl= Double.parseDouble(boltrow.getCell(0).toString())*1000000
            	+Double.parseDouble(boltrow.getCell(1).toString());
            	 
            	Integer num = dbl.intValue();
            	
            	if(m_smooth.getBooleanValue()) node1.put(dbl,Double.parseDouble(boltrow.getCell(3).toString()));
            	else node1.put(dbl,Double.parseDouble(boltrow.getCell(2).toString()));
            	//score1.add(Double.parseDouble(boltrow.getCell(2).toString()));
            	
            }
            
            System.out.println(inData[0].getRowCount());
         int count=0;   
         
         
         
         // the data table spec of the single output table, 
         // the table will have three columns:
         DataColumnSpec[] allColSpecs = new DataColumnSpec[4+targets.size()];
         allColSpecs[0] = 
             new DataColumnSpecCreator("Score", IntCell.TYPE).createSpec();
         
         allColSpecs[1] = 
             new DataColumnSpecCreator("Cmpd", StringCell.TYPE).createSpec();
         
         int a=2;
         for(Double targ:targets){
         allColSpecs[a] = 
             new DataColumnSpecCreator(targ.toString(), DoubleCell.TYPE).createSpec();
         ++a;}
         
         allColSpecs[targets.size()+2] = 
             new DataColumnSpecCreator("Sum", DoubleCell.TYPE).createSpec();
         
         allColSpecs[targets.size()+3] = 
             new DataColumnSpecCreator("FP", StringCell.TYPE).createSpec();
         
         DataTableSpec outputSpec = new DataTableSpec(allColSpecs);
         // the execution context will provide us with storage capacity, in this
         // case a data container to which we will add rows sequentially
         // Note, this container can also handle arbitrary big data tables, it
         // will buffer to disc if necessary.
         BufferedDataContainer container = exec.createDataContainer(outputSpec);
         // let's add m_count rows to it
         
         
         
         
         
         int i = 0;
         for(DataRow rgnRow : inData[0])
         {
            if(count>50000){
        	 System.out.print(".");
        	 count=0;
            }
            else ++ count;
            
        	 String s1 = rgnRow.getCell(0).toString();
          // 	strings.add(s1);
           	
           	String s2 = rgnRow.getCell(1).toString();
          // 	compounds.add(s2);
           	
           	if(testing) System.out.println("compound = "+s2);
           		
           	ArrayList<Double> fp1 =new ArrayList<Double>();
           	ArrayList<Double> tar =new ArrayList<Double>();
           	
           	
    		fp1.clear();
           	
    		StringTokenizer st1 = new StringTokenizer(s1,",");

			while(st1.hasMoreTokens())
			{
				fp1.add(Double.parseDouble(st1.nextToken()));
			}
    		int score =0;
			
			for(Double tid:targets)
    		{
				Double tarx =1.0;
				if(testing) System.out.println("target = "+tid);
				
				boolean isScore= false;
				for(Double id1:fp1)
    			{
										
					Double test = tid*1000000+
    				id1;
					Integer t1 = test.intValue();
    				
					if(testing){
						if(node1.containsKey(test))
						{
						System.out.print(id1+ ", "+test+" , ");
						System.out.println(node1.get(test));
						}
					}
    				if(node1.containsKey(test)) tarx=tarx*(1.0-node1.get(test));
    				if(tid.equals(id1)){
    					++score;
    					//tarx=0.0;
    					isScore=true;
    					
    				}
					//if(testing) System.out.println(tarx);
    				if(tarx>1.0) System.out.println(tarx);
    			}
				//System.out.println(tarx.toString());
				if(testing) System.out.println(tarx);
				tar.add(1.0-tarx);
				if(isScore){
					writer1.println(1.0-tarx);
				    ++counterr;
				    toterr=toterr+1.0-tarx;
				}
    		}
		//	scores.add(score);
		//	pout.add(tar);
			
			RowKey key = new RowKey("Row " + i);
            // the cells of the current row, the types of the cells must match
            // the column spec (see above)
            DataCell[] cells = new DataCell[4+targets.size()];
            cells[0] =  new IntCell(score); 
            
            cells[1] = new StringCell(s2);
            double sum =0;
            for(int b=2;b<=targets.size()+1;++b){
            cells[b] = new DoubleCell(tar.get(b-2));
            sum=sum+tar.get(b-2);
            }
            cells[targets.size()+2] = new DoubleCell(sum);           
            cells[targets.size()+3] = new StringCell(s1);
            
            DataRow row = new DefaultRow(key, cells);
            container.addRowToTable(row);
            
            // check if the execution monitor was canceled
            exec.checkCanceled();
			
			++i;
			
			//tar.clear();
			
			if(testing) System.out.println(count);
			if(testing) System.out.println(tar);
       	 }
            
         writer1.println(counterr+","+toterr+","+toterr/counterr);
            
         writer1.close();
         
         System.out.println("endmainloop");
         
       
        
       
        
        // once we are done, we close the container and return its table
        container.close();
        BufferedDataTable out = container.getTable();
        
        Date newdate1 = new Date();
		long time1 = newdate1.getTime();
		long delta1 = (time1 - starttime)/1000;

		System.out.println("Time was " + delta1+" s");
        
        
        return new BufferedDataTable[]{out};
        
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reset() {
        // TODO Code executed on reset.
        // Models build during execute are cleared here.
        // Also data handled in load/saveInternals will be erased here.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException {
        
        // TODO: check if user settings are available, fit to the incoming
        // table structure, and the incoming types are feasible for the node
        // to execute. If the node can execute in its current state return
        // the spec of its output data table(s) (if you can, otherwise an array
        // with null elements), or throw an exception with a useful user message

        return new DataTableSpec[]{null};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings) {

        // TODO save user settings to the config object.
        
    	m_col_1.saveSettingsTo(settings);
    	m_col_2.saveSettingsTo(settings);
    	m_smooth.saveSettingsTo(settings);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException {
            
        // TODO load (valid) settings from the config object.
        // It can be safely assumed that the settings are valided by the 
        // method below.
        
    	m_col_1.loadSettingsFrom(settings);
    	m_col_2.loadSettingsFrom(settings);
    	m_smooth.loadSettingsFrom(settings);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException {
            
        // TODO check if the settings could be applied to our model
        // e.g. if the count is in a certain range (which is ensured by the
        // SettingsModel).
        // Do not actually set any values of any member variables.

    	m_col_1.validateSettings(settings);
    	m_col_2.validateSettings(settings);
    	m_smooth.validateSettings(settings);

    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
        
        // TODO load internal data. 
        // Everything handed to output ports is loaded automatically (data
        // returned by the execute method, models loaded in loadModelContent,
        // and user settings set through loadSettingsFrom - is all taken care 
        // of). Load here only the other internals that need to be restored
        // (e.g. data used by the views).

    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
       
        // TODO save internal models. 
        // Everything written to output ports is saved automatically (data
        // returned by the execute method, models saved in the saveModelContent,
        // and user settings saved through saveSettingsTo - is all taken care 
        // of). Save here only the other internals that need to be preserved
        // (e.g. data used by the views).

    }

}

