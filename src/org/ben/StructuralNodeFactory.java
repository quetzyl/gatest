package org.ben;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "Structural" Node.
 * 
 *
 * @author Ben
 */
public class StructuralNodeFactory 
        extends NodeFactory<StructuralNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public StructuralNodeModel createNodeModel() {
        return new StructuralNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<StructuralNodeModel> createNodeView(final int viewIndex,
            final StructuralNodeModel nodeModel) {
        return new StructuralNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new StructuralNodeDialog();
    }

}

