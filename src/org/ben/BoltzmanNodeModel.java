package org.ben;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

import cern.colt.*;

import org.apache.commons.math.stat.regression.*;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.RowKey;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelIntegerBounded;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeLogger;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;


/**
 * This is the model implementation of Boltzman.
 * 
 *
 * @author 
 */
public class BoltzmanNodeModel extends NodeModel {

	// the logger instance
	private static final NodeLogger logger = NodeLogger
	.getLogger(BoltzmanNodeModel.class);


	public static final String CFG_COL_1 = "column id";


	public static final String CFG_ITERATIONS = "no of topscores";


	public static final String CFG_CUTOFF = "cutoff";


	private final SettingsModelString m_col_1 = 
		new SettingsModelString(CFG_COL_1, "col1");

	private SettingsModelInteger m_topscore = new SettingsModelInteger(
			CFG_ITERATIONS, 2);

	private SettingsModelInteger m_cutoff = new SettingsModelInteger(
			CFG_CUTOFF, 2);

	/**
	 * Constructor for the node model.
	 */
	protected BoltzmanNodeModel() {

		// TODO one incoming port and one outgoing port is assumed
		super(2, 1);
	}

	/* class LinkCount {
		private int count;


		private double id1;
		private double id2;

		public LinkCount(double id1, double id2){
			this.id1=id1;
			this.id2=id2;


		}

		public void setcount() {this.count =1;}
		public void addcount() {++count;}

	}

    class IndCount {
		private int count;
		private double id1;


		public IndCount( double id1){
			this.id1=id1;

		}

		public void setcount() {this.count =1;}
		public void addcount() {++count;}

	}
	 */
	class ProtProb {
		private Double prob;
		private Double prob2;
		private Double id1;
		private Double id2;


		public ProtProb( double id1,double id2,double prob,double prob2){
			this.id1=id1;
			this.id2=id2;
			this.prob=prob;
			this.prob2=prob2;
		}




	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
			final ExecutionContext exec) throws Exception {

		try{

			Date newdate = new Date();
			long starttime = newdate.getTime();
			
			SimpleDateFormat formatter = new SimpleDateFormat("yy.MM.dd.hh.mm");
			
			String output = formatter.format(starttime);

			int net_col_1 = inData[0].getDataTableSpec().findColumnIndex(
					m_col_1.getStringValue());

			PrintWriter writer1 = new PrintWriter(new FileWriter("D:/EclipseRunOutputs/gtsmoothing."+output+".txt"), true);
			PrintWriter writer2 = new PrintWriter(new FileWriter("D:/EclipseRunOutputs/gtsmoothing."+output+".plot.txt"), true);
			
			ArrayList<Double> temptargets = new ArrayList<Double>();
			ArrayList<Double> targets = new ArrayList<Double>();
			
			Boolean testing = false;

			for(DataRow t:inData[1])
			{
				String s1 = t.getCell(net_col_1).toString();

				StringTokenizer st1 = new StringTokenizer(s1,",");

				while(st1.hasMoreTokens())
				{
					temptargets.add(Double.parseDouble(st1.nextToken()));
				}

			}
			
			for(Double temp: temptargets)
			{
				if(!targets.contains(temp)) targets.add(temp);
			}
			


			ArrayList<String> strings =new ArrayList<String>();
			ArrayList<ArrayList<Double>> FPArray =new ArrayList<ArrayList<Double>>();

			for(DataRow rgnRow : inData[0])
			{
				String s1 = rgnRow.getCell(net_col_1).toString();
				strings.add(s1);
			}

			int initcount= strings.size();
			System.out.println("strings "+initcount);

			TreeSet<Double> ind = new TreeSet<Double>();
			//ArrayList<Integer> indcount = new ArrayList<Integer>();
			TreeMap<Double, Integer> indi =new TreeMap<Double, Integer>();

			Integer[] goodcount = new Integer[initcount+2];
			Integer[] goodcountz = new Integer[initcount+2];
			Double[] goodcountn = new Double[initcount+2];
			Arrays.fill(goodcount,0);
			Arrays.fill(goodcountn,0.0);
			
			mainloop:
				for(int i=0;i<initcount;++i)
				{

					if(strings.isEmpty())break mainloop;

					ArrayList<Double> fp1 =new ArrayList<Double>();

					fp1.clear();

					String s1 = strings.get(i);
					//System.out.println(s1);


					StringTokenizer st1 = new StringTokenizer(s1,",");

					while(st1.hasMoreTokens())
					{
						fp1.add(Double.parseDouble(st1.nextToken()));
					}

					FPArray.add(fp1);	

					//for(double test:fp1) System.out.print(test+";");
					//System.out.println();
					//System.out.println("fp "+fp1.size());

					int fpmax=m_cutoff.getIntValue();
					if (fpmax>fp1.size()) fpmax=fp1.size();

					for(int j =0;j<fp1.size();++j)
					{
						if(!ind.contains(fp1.get(j)))
							ind.add(fp1.get(j));


					}	
				}

			//int pmax= ind.size();
			System.out.println("ind size "+ind.size());

			TreeMap<Double,TreeMap<Double,Integer>> link = new TreeMap<Double,TreeMap<Double,Integer>>();

			Random generator = new Random();
			int counter=0;
			for(ArrayList<Double> fp1:FPArray)
			{
				boolean cut = false;
				int fpmax=m_cutoff.getIntValue();
				if (fpmax<fp1.size()) cut =true;
				if (fpmax>fp1.size()) fpmax=fp1.size();
				
				//System.out.println(fp1.size());
				
				++counter;
				if(counter>10000){
					
					System.out.print(".");
					counter=0;
				}

				for(int j =0;j<fpmax;++j)
				{	
					Double jj = 0.0;
					if(cut){
						jj = fp1.get(generator.nextInt(fp1.size()));
						fp1.remove(jj);
					}
					else{
						jj = fp1.get(j);
					}
					
					
					
					if(!link.containsKey(jj)) link.put(jj, new TreeMap<Double,Integer>());

					if(indi.containsKey(jj)) indi.put(jj,indi.get(jj)+1);
					else 
					{
						indi.put(jj,1);
					}
					
					//if(jj==6075.0) System.out.println("count= "+ indi.get(jj) );
					
					innerloop:
						for(Double targ:targets)
						{
							
							//if(targ>jj)break innerloop;
							if(jj.equals(targ)) continue innerloop;
							if(!fp1.contains(targ))continue innerloop;
							
							
							
							if(!link.get(jj).containsKey(targ))link.get(jj).put(targ,1);
							else link.get(jj).put(targ, link.get(jj).get(targ)+1);

							if(testing)System.out.println(jj+","+targ+","+link.get(jj).get(targ));
							
							++goodcount[link.get(jj).get(targ)];
							
							//if((targ==6081.0)&&(jj==6075.0)) System.out.println(indi.get(jj)+" ,"+link.get(jj).get(targ));
							//if((targ==6081.0)&&(indi.get(jj)<link.get(jj).get(targ))) System.out.println(indi.get(jj)+" ,"+link.get(jj).get(targ));
							
						}
				}


			}


			System.out.println("middle "+indi.size()+","+link.size());
			System.out.println("intersect "+intersection(ind,targets).size());
			double v = 0.000;

			
			if(testing){
				for(Double targ:ind)
				{
				System.out.print(targ +", ");
				
				}
				System.out.println();
			}
			
			System.out.println();
			System.out.println("v= " +v);
			
			/*PriorityQueue<ProtProb> best = new PriorityQueue<ProtProb>(ind.size()*targets.size(),new Comparator<Object>(){

				public int compare(final Object o1, final Object o2) {
					final ProtProb p1 = (ProtProb) o1;
					final ProtProb p2 = (ProtProb) o2;

					return p2.prob.compareTo(p1.prob); 

				}
			});*/
			int gctot =0;
			/*for(int h=goodcount.length-2;h>=0;--h){
				
				System.out.print(h+","+goodcount[h]+", ");
				//goodcountn[h]=goodcountn[h+1]+goodcount[h];
				goodcountn[h]=goodcount[h]-goodcount[h+1];
				gctot=gctot+goodcount[h];			
			}*/
			
			Integer first=0;
			Integer second=0;
			Integer third=0;
			Integer firsth=0;
			Integer secondh=0;
			Integer thirdh=0;
			
			SimpleRegression gtsr = new SimpleRegression();
			 
			for(int h=0;h<goodcount.length-1;++h){
				if(testing) System.out.print("i, "+h+","+goodcount[h]+", ");
				if(goodcount[h]>0.0) writer1.println(h+","+goodcount[h]+", ");
				
			 goodcountz[h]=goodcount[h]-goodcount[h+1];
			}
			System.out.println();
			writer1.println();
					
			gtsr.addData(Math.log(goodcountz[1]), Math.log(1.0));
			
			double calc =0.0;
			
			for(int h=1;h<goodcountz.length-1;++h){
				
				if(testing) System.out.print(h+","+goodcountz[h]+", ");
				if(goodcountz[h]>0.0) writer1.println("z, "+h+","+goodcountz[h]+", ");
								
				if(goodcountz[h]==0) {
					
					
					continue;
				}
				third=second;
				second=first;
				first=goodcountz[h];
				thirdh=secondh;
				secondh=firsth;
				firsth=h;
				if(third==0) continue;
				
				System.out.println("third= "+third+","+thirdh+",second="+second+","+secondh+",first="+first+","+firsth);
				
				calc=2*second.doubleValue()/(firsth.doubleValue()-thirdh.doubleValue());
				
				gtsr.addData(Math.log(calc), Math.log(secondh));
				
				writer2.println("third= "+third+","+thirdh+",second="+second+","+secondh+",first="+first+","+firsth+",inputs are "+calc+","+Math.log(2*second/(firsth-thirdh))+","+ Math.log(secondh));
				
				//goodcountn[h]= goodcount[h]-goodcount[h+1];
				gctot=gctot+goodcount[h];			
			}
			gtsr.addData(Math.log(first.doubleValue()/(firsth.doubleValue()-secondh.doubleValue())), Math.log(firsth));
			
			
			writer1.println();
			System.out.println();
			System.out.println(gctot);
			System.out.println();
			
			System.out.println(gtsr.getIntercept()+","+gtsr.getSlope());
			
			
			double gcntot =0;
			for(int h=1;h<goodcountn.length-1;++h){
				
				goodcountn[h]=Math.exp(gtsr.predict(Math.log(h)));
				
				
				if(testing) System.out.print(h+","+goodcountn[h]+", ");
				if(goodcountn[h]>0.0) writer1.println("n ,"+h+","+goodcountn[h]+", ");
				
				gcntot=gcntot+goodcountn[h];
			}
			
			System.out.println();
			System.out.println(gcntot);
			
			
			ArrayList<ProtProb> best = new ArrayList<ProtProb>();

			counter=0;
			//Integer bcount =1;
			for (Double i :ind) {
				if(testing)System.out.println(i);
				if(link.containsKey(i)){
					jloop:
					for(Double j : intersection(ind,targets)) {
						if(testing)System.out.println(i+","+j);
						if(i.equals(j)) continue jloop;
						if(link.get(i).containsKey(j)){

							/*int cji =0;
							
							if(link.containsKey(j)){
								if(link.get(j).containsKey(i)){
							    cji = link.get(j).get(i);		
								}
							}*/
							
							Integer c = //cji+
								link.get(i).get(j);
							
							
							
							
							// add one smoothing with v=1, Lidstones law with varied v
							
							Double t1 = (v+c.doubleValue())/
							(indi.get(i).doubleValue()+v*ind.size());
						/*															
							Double t2 = (v+c.doubleValue())/
							(indi.get(j).doubleValue()+v*ind.size());
*/
							// good turing smoothing
							
							Double gt1 = (c.doubleValue())*
							//((Math.max(1.0,goodcountn[c+1].doubleValue()))/(Math.max(1.0,goodcountn[c].doubleValue())))/
							((goodcountn[c+1].doubleValue())/(goodcountn[c].doubleValue()))/
							indi.get(i).doubleValue();
						/*	
							Double gt2 = (c.doubleValue())*
							((Math.max(1.0,goodcountn[c+1].doubleValue()))/(goodcountn[c].doubleValue()))/
							indi.get(j).doubleValue();*/
							
							if(testing)System.out.println(i+","+j+","+c+","+ indi.get(i).doubleValue()
									+","+gt1);
									//+","+ indi.get(j).doubleValue()+","+gt2);
							
							//BetaDistributionImpl d1 =new BetaDistributionImpl(
							//		indi.get(i).doubleValue()-link.get(i).get(j).doubleValue(),
							//		link.get(i).get(j).doubleValue()+1);
							//double t1= 1 -d1.inverseCumulativeProbability(0.25);

							/*if((bcount<=c))
							{
								bcount=bcount+10;
								System.out.println(c.doubleValue()+", i="+i+" ,"+
										indi.get(i).doubleValue() +",  j="+j+","
										+indi.get(j).doubleValue()
										+","+ t1 +","+ gt1);
										//+","+ t2+","+ gt2);
							}*/

					
							
							++counter;
							if(counter>10000){
								System.out.print(".");
								counter=0;
							}

							if(!t1.equals(0)){
								ProtProb p1 = new ProtProb(i,j,Math.min(1.0,t1.doubleValue()),Math.min(1.0,gt1.doubleValue()));
								best.add(p1);
							}
							
							/*if(!t2.equals(0)){
								ProtProb p2 = new ProtProb(j,i,Math.min(1.0,t2.doubleValue()),Math.min(10.0,gt2.doubleValue()));
								best.add(p2);
							}	*/
						}
					}
				}
				//else System.out.print("*");
			}

			//			Collections.sort(best, new Comparator<Object>(){
			//
			//				public int compare(final Object o1, final Object o2) {
			//					final ProtProb p1 = (ProtProb) o1;
			//					final ProtProb p2 = (ProtProb) o2;
			//
			//					return p2.prob.compareTo(p1.prob); 
			//
			//				}
			//			});

			System.out.println(best.size());
			Integer topscores = m_topscore.getIntValue();
			if(topscores<0) topscores=best.size();
			System.out.println(topscores);	



			DataColumnSpec[] allColSpecs = new DataColumnSpec[4];
			allColSpecs[0] = 
				new DataColumnSpecCreator("Protein A", DoubleCell.TYPE).createSpec();
			allColSpecs[1] = 
				new DataColumnSpecCreator("Protein B", DoubleCell.TYPE).createSpec();
			allColSpecs[2] = 
				new DataColumnSpecCreator("P(A|B)", DoubleCell.TYPE).createSpec();
			allColSpecs[3] = 
				new DataColumnSpecCreator("P(A|B)-GT", DoubleCell.TYPE).createSpec();

			DataTableSpec outputSpec = new DataTableSpec(allColSpecs);

			// the execution context will provide us with storage capacity, in this
			// case a data container to which we will add rows sequentially
			// Note, this container can also handle arbitrary big data tables, it
			// will buffer to disc if necessary.
			BufferedDataContainer container = exec.createDataContainer(outputSpec);
			// let's add m_count rows to it
			int rcount=0;

			for (ProtProb out:best) {


				RowKey key = new RowKey("Row " + rcount);
				++rcount;
				// the cells of the current row, the types of the cells must match
				// the column spec (see above)
				DataCell[] cells = new DataCell[4];
				cells[0] = new DoubleCell(out.id1);
				cells[1] = new DoubleCell(out.id2); 

				cells[2] = new DoubleCell(out.prob);
				cells[3] = new DoubleCell(out.prob2);

				DataRow row = new DefaultRow(key, cells);
				container.addRowToTable(row);

				// check if the execution monitor was canceled
				exec.checkCanceled();

			}
			// once we are done, we close the container and return its table
			container.close();
			BufferedDataTable out = container.getTable();

			writer1.close();
			writer2.close();

			Date newdate1 = new Date();
			long time1 = newdate1.getTime();
			long delta1 = (time1 - starttime)/1000;

			System.out.println("Time was " + delta1+" s");

			return new BufferedDataTable[]{out};


		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void reset() {
		// TODO Code executed on reset.
		// Models build during execute are cleared here.
		// Also data handled in load/saveInternals will be erased here.
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
	throws InvalidSettingsException {

		// TODO: check if user settings are available, fit to the incoming
		// table structure, and the incoming types are feasible for the node
		// to execute. If the node can execute in its current state return
		// the spec of its output data table(s) (if you can, otherwise an array
		// with null elements), or throw an exception with a useful user message

		return new DataTableSpec[]{null};
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings) {

		// TODO save user settings to the config object.

		m_col_1.saveSettingsTo(settings);
		m_cutoff.saveSettingsTo(settings);
		m_topscore.saveSettingsTo(settings);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
	throws InvalidSettingsException {

		// TODO load (valid) settings from the config object.
		// It can be safely assumed that the settings are valided by the 
		// method below.

		m_col_1.loadSettingsFrom(settings);
		m_cutoff.loadSettingsFrom(settings);
		m_topscore.loadSettingsFrom(settings);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validateSettings(final NodeSettingsRO settings)
	throws InvalidSettingsException {

		// TODO check if the settings could be applied to our model
		// e.g. if the count is in a certain range (which is ensured by the
		// SettingsModel).
		// Do not actually set any values of any member variables.

		m_col_1.validateSettings(settings);
		m_cutoff.validateSettings(settings);
		m_topscore.validateSettings(settings);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadInternals(final File internDir,
			final ExecutionMonitor exec) throws IOException,
			CanceledExecutionException {

		// TODO load internal data. 
		// Everything handed to output ports is loaded automatically (data
		// returned by the execute method, models loaded in loadModelContent,
		// and user settings set through loadSettingsFrom - is all taken care 
		// of). Load here only the other internals that need to be restored
		// (e.g. data used by the views).

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveInternals(final File internDir,
			final ExecutionMonitor exec) throws IOException,
			CanceledExecutionException {

		// TODO save internal models. 
		// Everything written to output ports is saved automatically (data
		// returned by the execute method, models saved in the saveModelContent,
		// and user settings saved through saveSettingsTo - is all taken care 
		// of). Save here only the other internals that need to be preserved
		// (e.g. data used by the views).

	}

	
	public static <T> Set<T> intersection(SortedSet<T> sortedSet, ArrayList<T> targets) {
	    Set<T> tmp = new TreeSet<T>();
	    for (T x : targets)
	      if (sortedSet.contains(x))
	        tmp.add(x);
	    return tmp;
	  }

}

