package org.ben;

import org.knime.core.node.NodeView;

/**
 * <code>NodeView</code> for the "FunctionPredictor" Node.
 * 
 *
 * @author neb
 */
public class FunctionPredictorNodeView extends NodeView<FunctionPredictorNodeModel> {

    /**
     * Creates a new view.
     * 
     * @param nodeModel The model (class: {@link FunctionPredictorNodeModel})
     */
    protected FunctionPredictorNodeView(final FunctionPredictorNodeModel nodeModel) {
        super(nodeModel);
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void modelChanged() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onClose() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onOpen() {
        // TODO: generated method stub
    }

}

