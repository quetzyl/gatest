package org.ben;

import java.util.Arrays;

import javax.swing.ListSelectionModel;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.DialogComponentStringListSelection;
import org.knime.core.node.defaultnodesettings.SettingsModelDouble;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelIntegerBounded;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.node.defaultnodesettings.SettingsModelStringArray;
import org.knime.core.data.StringValue;

/**
 * <code>NodeDialog</code> for the "BasicGA" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author 
 */
public class BasicGANodeDialog extends DefaultNodeSettingsPane {

    /**
     * New pane for configuring BasicGA node dialog.
     * This is just a suggestion to demonstrate possible default dialog
     * components.
     */
	private final SettingsModelString m_col_1 = 
		new SettingsModelString(BasicGANodeModel.CFG_COL_1, null);
	
	private final SettingsModelString m_col_2 = 
		new SettingsModelString(BasicGANodeModel.CFG_COL_2, null);
	
	private SettingsModelInteger m_size_of_pop = new SettingsModelInteger(
			BasicGANodeModel.CFG_POPULATION, 2);
	
	private SettingsModelInteger m_targetsubnet = new SettingsModelInteger(
			BasicGANodeModel.CFG_TARGET, 2);
	
	private SettingsModelInteger m_iterations = new SettingsModelInteger(
			BasicGANodeModel.CFG_ITERATIONS, 2);
		
	private SettingsModelDouble m_percentmutate = new SettingsModelDouble(
			BasicGANodeModel.CFG_PERCENTMUTATE, 2);
	
	private SettingsModelDouble m_sel_pres = new SettingsModelDouble(
			BasicGANodeModel.CFG_SELPRES, 2);
	
	private SettingsModelInteger m_scores = new SettingsModelInteger(
			BasicGANodeModel.CFG_SCORES, 2);
	
	private SettingsModelStringArray m_sfunctions = 
		new SettingsModelStringArray(BasicGANodeModel.CFG_SFUNCTIONS, null);
	
    protected BasicGANodeDialog() {
        super();
        
        addDialogComponent(
        		new DialogComponentColumnNameSelection(
        			m_col_1, "Column 1",
        			0, false, StringValue.class)
        	);
        
        addDialogComponent(
        		new DialogComponentColumnNameSelection(
        			m_col_2, "Column 2",
        			0, false, StringValue.class)
        	);
        
       // addDialogComponent(
       // 		new DialogComponentNumber(m_values, "No. of values to read in", 1)
       // 	);
        
        
        
        
        addDialogComponent(
        		new DialogComponentNumber(m_iterations, "Maximum number of iterations", 1)
        	);
        
        addDialogComponent(
        		new DialogComponentNumber(m_targetsubnet, "Goal length", 1)
        	);
        
        addDialogComponent(
            	new DialogComponentNumber(m_sel_pres, "Selection Pressure parameter ", 1)
           );
        
        addDialogComponent(
            	new DialogComponentNumber(m_size_of_pop, "Size of the evolving population ", 1)
           );
        
        addDialogComponent(
            	new DialogComponentNumber(m_percentmutate, "Percentage mutation (vs crossover) ", 1)
           );
        
        addDialogComponent(
        		new DialogComponentNumber(m_scores, "No. of scoring functions", 1)
        	);
        
        String[] list1 = {"AverageDegree","SumofDegree","MaxDegree"};
    	
        int num = list1.length;
        
        addDialogComponent(
        		new DialogComponentStringListSelection(m_sfunctions, "Properties to optimise", Arrays.asList(list1),
                    ListSelectionModel.MULTIPLE_INTERVAL_SELECTION, true, num)
        	);
        
    }
}

