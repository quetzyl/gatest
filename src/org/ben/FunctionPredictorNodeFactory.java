package org.ben;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "FunctionPredictor" Node.
 * 
 *
 * @author neb
 */
public class FunctionPredictorNodeFactory 
        extends NodeFactory<FunctionPredictorNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public FunctionPredictorNodeModel createNodeModel() {
        return new FunctionPredictorNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<FunctionPredictorNodeModel> createNodeView(final int viewIndex,
            final FunctionPredictorNodeModel nodeModel) {
        return new FunctionPredictorNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new FunctionPredictorNodeDialog();
    }

}

