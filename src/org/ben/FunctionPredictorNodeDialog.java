package org.ben;

import org.knime.core.data.DoubleValue;
import org.knime.core.data.StringValue;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentNumberEdit;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelDouble;
import org.knime.core.node.defaultnodesettings.SettingsModelString;

/**
 * <code>NodeDialog</code> for the "FunctionPredictor" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author neb
 */
public class FunctionPredictorNodeDialog extends DefaultNodeSettingsPane {

    /**
     * New pane for configuring Boltzman node dialog.
     * This is just a suggestion to demonstrate possible default dialog
     * components.
     */
	

    private final SettingsModelString m_col_1 = 
		new SettingsModelString(FunctionPredictorNodeModel.CFG_COL_1, null);
    
    private final SettingsModelString m_col_2 = 
		new SettingsModelString(FunctionPredictorNodeModel.CFG_COL_2, null);
    
    private final SettingsModelString m_col_4 = 
		new SettingsModelString(FunctionPredictorNodeModel.CFG_COL_4, null);
    
    private final SettingsModelString m_col_tgt = 
		new SettingsModelString(FunctionPredictorNodeModel.CFG_COL_TGT, null);
                
       
    private SettingsModelBoolean use_neg = new SettingsModelBoolean(
    		FunctionPredictorNodeModel.CFG_USE_NEG, false);
    
    private SettingsModelBoolean use_beta = new SettingsModelBoolean(
    		FunctionPredictorNodeModel.CFG_USE_BETA, false);
    
    private SettingsModelDouble m_error = new SettingsModelDouble(
    		FunctionPredictorNodeModel.CFG_ERROR, 2);
    
    private SettingsModelDouble m_adds = new SettingsModelDouble(
    		FunctionPredictorNodeModel.CFG_ADDS, 2);
    
    
    protected FunctionPredictorNodeDialog() {
        super();
        
        addDialogComponent(
        		new DialogComponentColumnNameSelection(
        			m_col_1, "Column containing compound ID",
        			0, false, StringValue.class)
        	);
        
        addDialogComponent(
        		new DialogComponentColumnNameSelection(
        			m_col_2, "1st Column containing binding data",
        			0, false, DoubleValue.class)
        	);
        
        addDialogComponent(
        		new DialogComponentColumnNameSelection(
        			m_col_4, "Last Column containing binding data",
        			0, false, DoubleValue.class)
        	);
        
        addDialogComponent(
        		new DialogComponentColumnNameSelection(
        			m_col_tgt, "Column containing target compound ID",
        			1, false, DoubleValue.class)
        	);
               
                
        addDialogComponent(
        		new DialogComponentBoolean( use_neg, "Use negative set of compounds" )
        	);
        
        addDialogComponent(
        		new DialogComponentBoolean( use_beta, "Use beta distribution" )
        	);
        
        addDialogComponent(
        		new DialogComponentNumberEdit(m_error, "Error correction [0 to 1]", 15)
        	);
        
        addDialogComponent(
        		new DialogComponentNumberEdit(m_adds, "Fraction of 50% prob added to normalise errors [0 to 1]", 15)
        	);
        
    }
}

