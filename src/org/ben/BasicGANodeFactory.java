package org.ben;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "BasicGA" Node.
 * 
 *
 * @author Ben
 */
public class BasicGANodeFactory 
        extends NodeFactory<BasicGANodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public BasicGANodeModel createNodeModel() {
        return new BasicGANodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<BasicGANodeModel> createNodeView(final int viewIndex,
            final BasicGANodeModel nodeModel) {
        return new BasicGANodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new BasicGANodeDialog();
    }

}

