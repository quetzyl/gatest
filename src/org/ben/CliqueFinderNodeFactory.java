package org.ben;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "CliqueFinder" Node.
 * 
 *
 * @author 
 */
public class CliqueFinderNodeFactory 
        extends NodeFactory<CliqueFinderNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public CliqueFinderNodeModel createNodeModel() {
        return new CliqueFinderNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<CliqueFinderNodeModel> createNodeView(final int viewIndex,
            final CliqueFinderNodeModel nodeModel) {
        return new CliqueFinderNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new CliqueFinderNodeDialog();
    }

}

