package org.ben;

import java.util.Arrays;

import javax.swing.JFileChooser;
import javax.swing.ListSelectionModel;

import org.knime.core.data.StringValue;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentFileChooser;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.DialogComponentStringListSelection;
import org.knime.core.node.defaultnodesettings.SettingsModelDouble;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelIntegerBounded;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.node.defaultnodesettings.SettingsModelStringArray;

/**
 * <code>NodeDialog</code> for the "JungGA" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author ben
 */
public class JungGANodeDialog extends DefaultNodeSettingsPane {

 
	private final SettingsModelString m_outputfile = 
		new SettingsModelString(JungGANodeModel.CFG_OUTFILE, null);
	
	private final SettingsModelString m_col_1 = 
    		new SettingsModelString(JungGANodeModel.CFG_COL_1, null);
    	
    	private final SettingsModelString m_col_2 = 
    		new SettingsModelString(JungGANodeModel.CFG_COL_2, null);
    	
    	private SettingsModelInteger m_size_of_pop = new SettingsModelInteger(
    			JungGANodeModel.CFG_POPULATION, 2);
    	
    	private SettingsModelInteger m_targetsubnet = new SettingsModelInteger(
    			JungGANodeModel.CFG_TARGET, 2);
    	
    	private SettingsModelInteger m_startsubnet = new SettingsModelInteger(
    			JungGANodeModel.CFG_START, 2);
    	
    	private SettingsModelInteger m_minsize = new SettingsModelInteger(
    			JungGANodeModel.CFG_MINSIZE, 2);
    	
    	private SettingsModelInteger m_iterations = new SettingsModelInteger(
    			JungGANodeModel.CFG_ITERATIONS, 2);
    		
    	private SettingsModelDouble m_percentmutate = new SettingsModelDouble(
    			JungGANodeModel.CFG_PERCENTMUTATE, 2);
    	
    	private SettingsModelDouble m_sel_pres = new SettingsModelDouble(
    			JungGANodeModel.CFG_SELPRES, 2);
    	
    	private SettingsModelInteger m_scores = new SettingsModelInteger(
    			JungGANodeModel.CFG_SCORES, 2);
    	
    	private SettingsModelStringArray m_sfunctions = 
    		new SettingsModelStringArray(JungGANodeModel.CFG_SFUNCTIONS, null);
    	
        protected JungGANodeDialog() {
            super();
            
            addDialogComponent(
            		new DialogComponentColumnNameSelection(
            			m_col_1, "Column 1",
            			0, false, StringValue.class)
            	);
            
            addDialogComponent(
            		new DialogComponentColumnNameSelection(
            			m_col_2, "Column 2",
            			0, false, StringValue.class)
            	);
            
           // addDialogComponent(
           // 		new DialogComponentNumber(m_values, "No. of values to read in", 1)
           // 	);
            String history = " ";
            
            addDialogComponent(
            		new DialogComponentFileChooser(m_outputfile,history, JFileChooser.SAVE_DIALOG, true)
            	);
            
            
            addDialogComponent(
            		new DialogComponentNumber(m_iterations, "Maximum number of iterations", 1)
            	);
            
            addDialogComponent(
            		new DialogComponentNumber(m_minsize, "Minimum permitted subnet size", 1)
            	);
            
            addDialogComponent(
            		new DialogComponentNumber(m_targetsubnet, "Maximum mutatable size", 1)
            	);
            
            addDialogComponent(
            		new DialogComponentNumber(m_startsubnet, "Starting length", 1)
            	);
            
            addDialogComponent(
                	new DialogComponentNumber(m_sel_pres, "Selection Pressure parameter ", 1)
               );
            
            addDialogComponent(
                	new DialogComponentNumber(m_size_of_pop, "Size of the evolving population ", 1)
               );
            
            addDialogComponent(
                	new DialogComponentNumber(m_percentmutate, "Percentage mutation (vs crossover) ", 1)
               );
            
            addDialogComponent(
            		new DialogComponentNumber(m_scores, "No. of scoring functions", 1)
            	);
            
            String[] list1 = {"AvPathLen","SumEigenCent","AVEigenCent","SumBetweenness",
            		"AvBetweenness","Diameter","SubnetSize","AverageDegree",
            		"SumofDegree","MaxDegree"};
        	
            int num = list1.length;
            
            addDialogComponent(
            		new DialogComponentStringListSelection(m_sfunctions, "Properties to optimise", Arrays.asList(list1),
                        ListSelectionModel.MULTIPLE_INTERVAL_SELECTION, true, num)
            	);
            
        }
}

