package org.ben;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "BFSim" Node.
 * 
 *
 * @author 
 */
public class BFSimNodeFactory 
        extends NodeFactory<BFSimNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public BFSimNodeModel createNodeModel() {
        return new BFSimNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<BFSimNodeModel> createNodeView(final int viewIndex,
            final BFSimNodeModel nodeModel) {
        return new BFSimNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new BFSimNodeDialog();
    }

}

