package org.ben;

import org.knime.core.data.StringValue;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.DialogComponentNumberEdit;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelIntegerBounded;
import org.knime.core.node.defaultnodesettings.SettingsModelString;

/**
 * <code>NodeDialog</code> for the "Boltzman" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author 
 */
public class StructuralNodeDialog extends DefaultNodeSettingsPane {

    /**
     * New pane for configuring Boltzman node dialog.
     * This is just a suggestion to demonstrate possible default dialog
     * components.
     */
	

    private final SettingsModelString m_col_1 = 
		new SettingsModelString(StructuralNodeModel.CFG_COL_1, null);
    
    private final SettingsModelString m_col_2 = 
		new SettingsModelString(StructuralNodeModel.CFG_COL_2, null);
                
//    private final SettingsModelString m_struct_col = 
//		new SettingsModelString(StructuralNodeModel.CFG_STRUCT_COL, null);
    
 //   private final SettingsModelString m_neg_col = 
//		new SettingsModelString(StructuralNodeModel.CFG_NEG_COL, null);
    
    
    private SettingsModelInteger m_topscore = new SettingsModelInteger(
    		StructuralNodeModel.CFG_ITERATIONS, 2);
    
    private SettingsModelInteger m_cutoff = new SettingsModelInteger(
    		StructuralNodeModel.CFG_CUTOFF, 2);
    
 //   private SettingsModelBoolean m_structural = new SettingsModelBoolean(
 //   		StructuralNodeModel.CFG_STRUCTURAL,null);
    
  //  private SettingsModelBoolean m_negative = new SettingsModelBoolean(
  //  		StructuralNodeModel.CFG_NEGATIVE,null);
    
    @SuppressWarnings("unchecked")
	protected StructuralNodeDialog() {
        super();
        
        
        addDialogComponent(
        		new DialogComponentColumnNameSelection(
        			m_col_1, "Binding IDs",
        			1, false, StringValue.class)
        	);
        
        addDialogComponent(
        		new DialogComponentColumnNameSelection(
        			m_col_2, "Fingerprint",
        			1, false, StringValue.class)
        	);
        
        
 //      addDialogComponent(
 //      		new DialogComponentBoolean(m_structural, "Do we have structural data (in input 3)")
  //      	);
        
 //       addDialogComponent(
  //      		new DialogComponentColumnNameSelection(
  //      			m_struct_col, "Structural fingerprints",
  //      			2, false, StringValue.class)
   //     	);
        
  //      addDialogComponent(
  //      		new DialogComponentBoolean(m_negative, "Do we have negative data (in input 4)")
  //      	);
        
             
 //       addDialogComponent(
   //     		new DialogComponentColumnNameSelection(
   //     			m_neg_col, "Negative binding IDs",
    //    			3, false, StringValue.class)
    //    	);
        
        addDialogComponent(
        		new DialogComponentNumberEdit(m_topscore, "No. of top scores to use (-1 for all)", 15)
        	);
        
        addDialogComponent(
        		new DialogComponentNumberEdit(m_cutoff, "FingerPrint length cutoff (<10000)", 15)
        	);
        
    }
}

