package org.ben;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.RowKey;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.defaultnodesettings.SettingsModelDouble;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelIntegerBounded;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.node.defaultnodesettings.SettingsModelStringArray;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeLogger;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import edu.uci.ics.jung.algorithms.shortestpath.DistanceStatistics;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseGraph;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;




/**
 * This is the model implementation of CliqueFinder.
 * 
 *
 * @author 
 */
public class CliqueFinderNodeModel extends NodeModel {

	// the logger instance
	private static final NodeLogger logger = NodeLogger
	.getLogger(CliqueFinderNodeModel.class);



	public static final String CFG_COL_1 = "Col1";

	public static final String CFG_COL_2 = "Col2";


	

	public static final String CFG_OUTFILE = "file name to write output";

	//public static final String CFG_NOPROPS = "no. of scoring functions";

	private final SettingsModelString m_outputfile = 
		new SettingsModelString(CFG_OUTFILE, "default");
	
	private final SettingsModelString m_col_1 = 
		new SettingsModelString(CFG_COL_1, "col1");

	private final SettingsModelString m_col_2 = 
		new SettingsModelString(CFG_COL_2, "col2");

	


	/**
	 * Constructor for the node model.
	 */
	protected CliqueFinderNodeModel() {

		super(1, 1);
	}

	class E {
		private int idno;
		
		public E(int idno){
			this.idno=idno;
		}
		public String toString() { 	// Always a good idea for debuging
			return "E"+idno; 	}	// JUNG2 makes good use of these.
		
	}
	
	class V {
		private String nodeid;
		

		public V(String nodeid){
			this.nodeid=nodeid;
			
		}
		
		public String getnodeid() {return nodeid;}
		public String toString() { 	// Always a good idea for debuging
			return "V"+nodeid; 	}	// JUNG2 makes good use of these.

		
	}
	
	


    

	


	/**
	 * {@inheritDoc}
	 */
	 @Override
	 protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
			 final ExecutionContext exec) throws Exception {

		 try{
		
			Date newdate = new Date();
			long starttime = newdate.getTime();

			PrintWriter writer1 = new PrintWriter(new FileWriter(m_outputfile.getStringValue()), true);
		 //GetNetwork();
		 //GenerateNodeList();
		 //CalculateDegree();

		 int net_col_1 = inData[0].getDataTableSpec().findColumnIndex(
				 m_col_1.getStringValue());
		 int net_col_2 = inData[0].getDataTableSpec().findColumnIndex(
				 m_col_2.getStringValue());
		 
		// System.out.println("column index "+inData[0].getDataTableSpec().findColumnIndex(
		//		 m_col_2.getStringValue()));

		

	     
		 SparseGraph<V,E> graph = new SparseGraph<V,E>();

		
		 
		 int ecount =0;
				 
		 for (DataRow rgnRow : inData[0])
		 {
			 exec.checkCanceled();
			 String s1 = rgnRow.getCell(net_col_1).toString();
			 String s2 = rgnRow.getCell(net_col_2).toString();
			 
			 boolean p1=false;
			 boolean p2=false;
			 
			 V v1 = new V(s1);
			 V v2 = new V(s2);
			 			 
			 for (V test: graph.getVertices())
			 {
				 if(test.getnodeid().equals(s1)) v1=test;
				 if(test.getnodeid().equals(s2)) v2=test;
			 }
			 			 
			 E e =new E(ecount);
						
			 graph.addEdge(e, v1, v2);
			 ++ecount;
			 
		 }
		 
		 //System.out.print(graph.toString());
		
		 System.out.println("no. of nodes is "+graph.getVertexCount());
		 System.out.println("no. of edges is "+graph.getEdgeCount()+" , "+ecount);
		 System.out.println("diameter is "+DistanceStatistics.diameter(graph));
		 
		 Collection<Set<V>> cliques = getAllMaximalCliques( graph);
		
		
		 
		 
		 //Output final results set
		 
		
		 	


			 // the data table spec of the single output table, 
			 // the table will have three columns:
			 DataColumnSpec[] allColSpecs = new DataColumnSpec[2];
			 allColSpecs[0] = 
				 new DataColumnSpecCreator("cliques", StringCell.TYPE).createSpec();
			 allColSpecs[1] = 
				 new DataColumnSpecCreator("Length", IntCell.TYPE).createSpec();
			 
			 
			 
			 DataTableSpec outputSpec = new DataTableSpec(allColSpecs);
			 // the execution context will provide us with storage capacity, in this
			 // case a data container to which we will add rows sequentially
			 // Note, this container can also handle arbitrary big data tables, it
			 // will buffer to disc if necessary.
			 BufferedDataContainer container = exec.createDataContainer(outputSpec);
			 // let's add m_count rows to it
			 int i=0;
			 for (Set<V> clique:cliques) {
				 
				 
				 
				 //for(Node test:subset) System.out.print(test.getnodeid()+" ");
				 //System.out.println();	
				 String	subsettostring="";
				for(V v:clique)
				{
					subsettostring=subsettostring.concat(v.getnodeid()+",");
				}	
				 RowKey key = new RowKey("Row " + i);
				 // the cells of the current row, the types of the cells must match
				 // the column spec (see above)
				 DataCell[] cells = new DataCell[2];
				 cells[0] = new StringCell(subsettostring); 
				 cells[1] = new IntCell(clique.size()); 
				 
				 
				 DataRow row = new DefaultRow(key, cells);
				 container.addRowToTable(row);

				 // check if the execution monitor was canceled
				 exec.checkCanceled();
				 exec.setProgress(i / (double)3, 
						 "Adding row " + i);
				 ++i;
			 }
			 // once we are done, we close the container and return its table
			 container.close();
			 BufferedDataTable out = container.getTable();
			
			 
		Date newdate2 = new Date();
		long time2 = newdate2.getTime();
		long delta2 = (time2 - starttime)/1000;

		System.out.println("Total time was " + delta2+" s");
					
		return new BufferedDataTable[]{out};
			 
	 } catch (Exception ex) {
		            ex.printStackTrace();
		            throw ex;
		        }
	 }
	 
	 
		 /**
		  * {@inheritDoc}
		  */
		 @Override
		 protected void reset() {
			 // TODO Code executed on reset.
			 // Models build during execute are cleared here.
			 // Also data handled in load/saveInternals will be erased here.
		 }

		 /**
		  * {@inheritDoc}
		  */
		 @Override
		 protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
		 throws InvalidSettingsException {

			 // TODO: check if user settings are available, fit to the incoming
			 // table structure, and the incoming types are feasible for the node
			 // to execute. If the node can execute in its current state return
			 // the spec of its output data table(s) (if you can, otherwise an array
			 // with null elements), or throw an exception with a useful user message

			 return new DataTableSpec[]{null};
		 }

		 /**
		  * {@inheritDoc}
		  */
		 @Override
		 protected void saveSettingsTo(final NodeSettingsWO settings) {

			 // TODO save user settings to the config object.


			 m_col_1.saveSettingsTo(settings);
			 m_col_2.saveSettingsTo(settings);
			 m_outputfile.saveSettingsTo(settings);
		
		 }

		 /**
		  * {@inheritDoc}
		  */
		 @Override
		 protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
		 throws InvalidSettingsException {

			 // TODO load (valid) settings from the config object.
			 // It can be safely assumed that the settings are valided by the 
			 // method below.


			 m_col_1.loadSettingsFrom(settings);
			 m_col_2.loadSettingsFrom(settings);
			 m_outputfile.loadSettingsFrom(settings);
		
		 }

		 /**
		  * {@inheritDoc}
		  */
		 @Override
		 protected void validateSettings(final NodeSettingsRO settings)
		 throws InvalidSettingsException {

			 // TODO check if the settings could be applied to our model
			 // e.g. if the count is in a certain range (which is ensured by the
			 // SettingsModel).
			 // Do not actually set any values of any member variables.


			 m_col_1.validateSettings(settings);
			 m_col_2.validateSettings(settings);
			 m_outputfile.validateSettings(settings);
			
		 }

		 /**
		  * {@inheritDoc}
		  */
		 @Override
		 protected void loadInternals(final File internDir,
				 final ExecutionMonitor exec) throws IOException,
				 CanceledExecutionException {

			 // TODO load internal data. 
			 // Everything handed to output ports is loaded automatically (data
			 // returned by the execute method, models loaded in loadModelContent,
			 // and user settings set through loadSettingsFrom - is all taken care 
			 // of). Load here only the other internals that need to be restored
			 // (e.g. data used by the views).

		 }

		 /**
		  * {@inheritDoc}
		  */
		 @Override
		 protected void saveInternals(final File internDir,
				 final ExecutionMonitor exec) throws IOException,
				 CanceledExecutionException {

			 // TODO save internal models. 
			 // Everything written to output ports is saved automatically (data
			 // returned by the execute method, models saved in the saveModelContent,
			 // and user settings saved through saveSettingsTo - is all taken care 
			 // of). Save here only the other internals that need to be preserved
			 // (e.g. data used by the views).

		 }
		     public Collection<Set<V>> getAllMaximalCliques(Graph<V,E> graph)
		     {
		         // TODO:  assert that graph is simple

		    	 Collection<Set<V>> cliques = new ArrayList<Set<V>>();
		         List<V> potential_clique = new ArrayList<V>();
		         List<V> candidates = new ArrayList<V>();
		         List<V> already_found = new ArrayList<V>();
		         candidates.addAll(graph.getVertices());
		         findCliques(graph,cliques,potential_clique, candidates, already_found);
		         return cliques;
		     }

		     /**
		      * Finds the biggest maximal cliques of the graph.
		      */
		     public Collection<Set<V>> getBiggestMaximalCliques(Graph<V,E> graph)
		     {
		         // first, find all cliques
		    	 Collection<Set<V>> cliques = getAllMaximalCliques(graph);

		         int maximum = 0;
		         Collection<Set<V>> biggest_cliques = new ArrayList<Set<V>>();
		         for (Set<V> clique : cliques) {
		             if (maximum < clique.size()) {
		                 maximum = clique.size();
		             }
		         }
		         for (Set<V> clique : cliques) {
		             if (maximum == clique.size()) {
		                 biggest_cliques.add(clique);
		             }
		         }
		         return biggest_cliques;
		     }

		     private void findCliques(Graph<V,E> graph,Collection<Set<V>> cliques,
		         List<V> potential_clique,
		         List<V> candidates,
		         List<V> already_found)
		     {
		         List<V> candidates_array = new ArrayList<V>(candidates);
		         if (!end(graph,candidates, already_found)) {
		             // for each candidate_node in candidates do
		             for (V candidate : candidates_array) {
		                 List<V> new_candidates = new ArrayList<V>();
		                 List<V> new_already_found = new ArrayList<V>();

		                 // move candidate node to potential_clique
		                 potential_clique.add(candidate);
		                 candidates.remove(candidate);

		                 // create new_candidates by removing nodes in candidates not
		                 // connected to candidate node
		                 for (V new_candidate : candidates) {
		                     if (graph.isNeighbor(candidate, new_candidate))
		                     {
		                         new_candidates.add(new_candidate);
		                     } // of if
		                 } // of for

		                 // create new_already_found by removing nodes in already_found
		                 // not connected to candidate node
		                 for (V new_found : already_found) {
		                     if (graph.isNeighbor(candidate, new_found)) {
		                         new_already_found.add(new_found);
		                     } // of if
		                 } // of for

		                 // if new_candidates and new_already_found are empty
		                 if (new_candidates.isEmpty() && new_already_found.isEmpty()) {
		                     // potential_clique is maximal_clique
		                     cliques.add(new HashSet<V>(potential_clique));
		                 } // of if
		                 else {
		                     // recursive call
		                     findCliques(graph,cliques,
		                         potential_clique,
		                         new_candidates,
		                         new_already_found);
		                 } // of else

		                 // move candidate_node from potential_clique to already_found;
		                 already_found.add(candidate);
		                 potential_clique.remove(candidate);
		             } // of for
		         } // of if
		     }

		     private boolean end(Graph<V,E> graph,List<V> candidates, List<V> already_found)
		     {
		         // if a node in already_found is connected to all nodes in candidates
		         boolean end = false;
		         int edgecounter;
		         for (V found : already_found) {
		             edgecounter = 0;
		             for (V candidate : candidates) {
		                 if (graph.isNeighbor(found, candidate)) {
		                     edgecounter++;
		                 } // of if
		             } // of for
		             if (edgecounter == candidates.size()) {
		                 end = true;
		             }
		         } // of for
		         return end;
		     }
		 }



		 
	 
