package org.ben;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnFilter;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.SettingsModelFilterString;
import org.knime.core.node.defaultnodesettings.SettingsModelIntegerBounded;
import org.knime.core.node.defaultnodesettings.SettingsModelString;

/**
 * <code>NodeDialog</code> for the "Pareto" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author 
 */


public class ParetoNodeDialog extends DefaultNodeSettingsPane {

    /**
     * New pane for configuring Pareto node dialog.
     * This is just a suggestion to demonstrate possible default dialog
     * components.
     */
	
	private final SettingsModelFilterString m_col_1 = 
		new SettingsModelFilterString(ParetoNodeModel.CFG_COL_1);

	
    protected ParetoNodeDialog() {
        super();
        
        addDialogComponent(new DialogComponentColumnFilter(m_col_1,0,true));
                
                    
    }
}

